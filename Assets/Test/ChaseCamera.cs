﻿using UnityEngine;
using System.Collections;

public class ChaseCamera : MonoBehaviour
{
    public GameObject target;
    private Vector3 offset = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        offset = transform.position - target.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 newPositon = transform.position;
        newPositon.x = target.transform.position.x + offset.x;
        newPositon.y = target.transform.position.y + offset.y;
        newPositon.z = target.transform.position.z + offset.z;

        //transform.position = Vector3.Lerp(transform.position, newPositon, 5.0f * Time.deltaTime);
        transform.position = newPositon;
    }
}
