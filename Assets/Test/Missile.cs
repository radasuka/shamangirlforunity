﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Missile : MonoBehaviour
{
    public GameObject target;
    private Rigidbody _rigidbody;

    private Vector3 _upEngine = Vector3.up * 0.5f;
    private Vector3 _downEngine = Vector3.down * 0.5f;
    private Vector3 _leftEngine = Vector3.left * 0.5f;
    private Vector3 _rightEngine = Vector3.right * 0.5f;

    public float EnginePower = 10.0f;

    // Use this for initialization
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dif = target.transform.position - transform.position;
        Vector3 difLocal = transform.InverseTransformVector(dif);

        float upPower;
        float downPower;
        float leftPower;
        float rightPower;

        float difX = difLocal.x;
        if(difX > 0)
        {
            leftPower = 1.1f;
            rightPower = 0.9f;
        }
        else if(difX < 0)
        {
            leftPower = 0.9f;
            rightPower = 1.1f;
        }
        else
        {
            leftPower = rightPower = 1.0f;
        }

        float difY = difLocal.y;
        if (difY > 0)
        {
            upPower = 0.9f;
            downPower = 1.1f;
        }
        else if (difY < 0)
        {
            upPower = 1.1f;
            downPower = 0.9f;
        }
        else
        {
            upPower = downPower = 1.0f;
        }

        Vector3 forward = transform.forward;
        _rigidbody.AddForceAtPosition(EnginePower * upPower * forward, transform.TransformPoint(_upEngine), ForceMode.Force);
        _rigidbody.AddForceAtPosition(EnginePower * downPower * forward, transform.TransformPoint(_downEngine), ForceMode.Force);
        _rigidbody.AddForceAtPosition(EnginePower * leftPower * forward, transform.TransformPoint(_leftEngine), ForceMode.Force);
        _rigidbody.AddForceAtPosition(EnginePower * rightPower * forward, transform.TransformPoint(_rightEngine), ForceMode.Force);
    }
}
