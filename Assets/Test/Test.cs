﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Test : MonoBehaviour
{
    Coroutine cor;
    bool isRun = false;
    int sel;

    private delegate IEnumerator Func();
    private Func func;
    private List<Func> funcs = new List<Func>();

    void Awake()
    {
        funcs.Add(Roll);
        funcs.Add(Scall);
    }

    // Use this for initialization
    void Start()
    {
        sel = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (isRun)
            {
                StopCoroutine(cor);
                isRun = false;
            }
            else
            {
                cor = StartCoroutine(funcs[sel]());
                isRun = true;
                if (sel == 1)
                    sel = 0;
                else
                    sel++;
            }
        }
    }

    private IEnumerator Roll()
    {
        float y = transform.rotation.y;
        while (true)
        {
            y += 1;
            transform.localRotation = Quaternion.Euler(0, y, 0);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator Scall()
    {
        float val = 0;
        Vector3 scall = transform.localScale;
        while (true)
        {
            val = Mathf.Sin(10 * Time.time) + 1.5f;
            scall = new Vector3(val, val, val);
            transform.localScale = scall;

            yield return new WaitForEndOfFrame();
        }
    }

    private void OnGUI()
    {
        Utility.SetFontAlignment(TextAnchor.MiddleLeft);
        Utility.SetFontSize(20);
        Utility.GUILabel(10, 10, 1, 1, isRun.ToString());
        Utility.GUILabel(10, 40, 1, 1, funcs[sel].ToString());
        Utility.GUILabel(10, 70, 1, 1, Time.frameCount.ToString());
    }
}
