﻿using UnityEngine;
using System.Collections;

public class TitleManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        // サウンドロード
        Sound.LoadBgm("BGM", "BGM/Title");
        Sound.LoadSe("Enter", "SE/DM-CGS-45");
        Sound.PlayBgm("BGM");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GameStart()
    {
        FadeManager.Instance.LoadLevel("PlayGameScene", 1f, Color.black, new Rect(0, 0, Screen.width, Screen.height));
        Sound.PlaySe("Enter");
    }

    public void EndGame()
    {
        Application.Quit();
        Sound.PlaySe("Enter");
    }
}
