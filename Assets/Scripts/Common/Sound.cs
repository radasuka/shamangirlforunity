﻿using System.Collections.Generic;
using UnityEngine;

public class Sound
{
    // SEチャンネル数
    private const int SE_CHANNEL = 4;
    // サウンド種別
    private enum eType
    {
        Bgm,
        Se
    }

    // シングルトン
    private static Sound singletonInstance = null;
    /// <summary>
    /// インスタンス取得
    /// </summary>
    /// <returns></returns>
    public static Sound GetInstance()
    {
        return singletonInstance ?? (singletonInstance = new Sound());
    }

    // サウンド再生のためのゲームオブジェクト
    GameObject _object = null;
    // サウンドリソース
    AudioSource sourceBgm = null;
    AudioSource sourceSeDefault = null;
    AudioSource[] sourceSeArray;
    // BGMにアクセスするためのテーブル
    Dictionary<string, _Data> poolBgm = new Dictionary<string, _Data>();
    // SEにアクセスするためのテーブル
    Dictionary<string, _Data> poolSe = new Dictionary<string, _Data>();

    class _Data
    {
        // アクセス用のキー
        public string Key;
        // リソース名
        public string ResName;
        // AudioClip
        public AudioClip Clip;

        public _Data(string key, string res)
        {
            Key = key;
            ResName = "Sounds/" + res;
            // AudioClipの取得
            Clip = Resources.Load(ResName) as AudioClip;
        }
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public Sound()
    {
        // チャンネル確保
        sourceSeArray = new AudioSource[SE_CHANNEL];
    }

    // AudioSourceを取得する
    private AudioSource GetAudioSource(eType type, int channel = -1)
    {
        if(_object == null)
        {
            // GameObjectがなければ作る
            _object = new GameObject("Sound");
            // 破棄しないようにする
            GameObject.DontDestroyOnLoad(_object);

            // AudioSourceを作成
            sourceBgm = _object.AddComponent<AudioSource>();
            sourceBgm.spatialBlend = 0; // 2Dサウンドを有効
            sourceSeDefault = _object.AddComponent<AudioSource>();
            sourceSeDefault.spatialBlend = 0;
            for (int index = 0; index < SE_CHANNEL; ++index)
            {
                sourceSeArray[index] = _object.AddComponent<AudioSource>();
                sourceSeArray[index].spatialBlend = 0;
            }
        }

        if(type == eType.Bgm)
        {
            // BGM
            return sourceBgm;
        }
        else
        {
            if (0 <= channel && channel < SE_CHANNEL)
                // チャンネル指定
                return sourceSeArray[channel];
            else
                return sourceSeDefault;
        }
    }

    /// <summary>
    /// サウンドのロード
    /// ※Resources/Soundsフォルダに配置すること
    /// </summary>
    /// <param name="key"></param>
    /// <param name="resName"></param>
    public static void LoadBgm(string key, string resName)
    {

        GetInstance()._LoadBgm(key, resName);
    }

    public static void LoadSe(string key, string resName)
    {
        GetInstance()._LoadSe(key, resName);
    }

    private void _LoadBgm(string key, string resName)
    {
        if (poolBgm.ContainsKey(key))
            // すでに登録済みなのでいったん消す
            poolBgm.Remove(key);

        poolBgm.Add(key, new _Data(key, resName));
    }
    private void _LoadSe(string key, string resName)
    {
        if (poolBgm.ContainsKey(key))
            // すでに登録済みなのでいったん消す
            poolSe.Remove(key);

        poolSe.Add(key, new _Data(key, resName));
    }

    /// <summary>
    /// BGMの再生
    /// ※事前にLoadBgmでロードしておくこと
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public static bool PlayBgm(string key)
    {
        return GetInstance()._PlayBgm(key);
    }

    private bool _PlayBgm(string key)
    {
        if (poolBgm.ContainsKey(key) == false)
            // 対応するキーがない
            return false;

        // いったん止める
        _StopBgm();

        // リソースの取得
        var data = poolBgm[key];

        // 再生
        var source = GetAudioSource(eType.Bgm);
        source.loop = true;
        source.clip = data.Clip;
        source.volume = 1;
        source.Play();

        return true;
    }

    /// <summary>
    /// BGMの停止
    /// </summary>
    /// <returns></returns>
    public static bool StopBgm()
    {
        return GetInstance()._StopBgm();
    }

    private bool _StopBgm()
    {
        GetAudioSource(eType.Bgm).Stop();
        return true;
    }

    /// <summary>
    /// SEの再生
    /// ※事前にLoadSeでロードしておくこと
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public static bool PlaySe(string key, int channel = -1)
    {
        return GetInstance()._PlaySe(key, channel);
    }

    private bool _PlaySe(string key, int channel = -1)
    {
        if (poolSe.ContainsKey(key) == false)
            // 対応するキーがない
            return false;

        // リソースの取得
        var data = poolSe[key];

        // 再生
        if(0 <= channel && channel < SE_CHANNEL)
        {
            // チャンネル指定
            var source = GetAudioSource(eType.Se, channel);
            source.clip = data.Clip;
            source.Play();
        }
        else
        {
            // デフォルトで再生
            var source = GetAudioSource(eType.Se);
            source.PlayOneShot(data.Clip, 0.9f);
        }

        return true;
    }
}

