﻿public interface IState<T>
{
    void Update(float elapsedTime, T item);
}
