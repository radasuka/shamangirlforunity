﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class CSVReader
{
    /// <summary>
    /// CSVファイルの拡張子
    /// </summary>
    public const string Extension = ".csv";
    /// <summary>
    /// セルを区切る為にキャラ
    /// </summary>
    public const char Split = ',';

    /// <summary>
    /// 行がコメントだと言う事を表す文字列
    /// </summary>
    private string commentString = "//";

    /// <summary>
    /// このテーブルに属する行のコレクションを取得します
    /// </summary>
    private List<List<string>> Rows = new List<List<string>>();

    private TextAsset textAsset = null;
    private bool isReadFromDisc;

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="comment">コメントの文字列</param>
    /// <param name="isReadFromDisc">Resourcesでなくディスク(フルパス)から読み込むか</param>
    public CSVReader(string comment = "//", bool isReadFromDisc = false)
    {
        textAsset = null;
        Rows = new List<List<string>>();
        commentString = comment;
        this.isReadFromDisc = isReadFromDisc;
    }

    /// <summary>
    /// CSV読み込み
    /// </summary>
    /// <param name="fileName">ファイル名</param>
    /// <returns></returns>
    public bool Load(string fileName)
    {
        Rows.Clear();

        TextReader textReader = CreateTextReader(fileName);

        int counter = 0;
        string line = "";

        while((line = textReader.ReadLine()) != null)
        {
            // コメントが入っている時はスキップする
            if (line.Contains(commentString))
                continue;

            // 今の列をマス毎に区切る
            string[] fields = line.Split(Split);
            Rows.Add(new List<string>());

            foreach(var field in fields)
            {
                if (field.Contains(commentString) || field == "")
                    continue;

                Rows[counter].Add(field);
            }
            counter++;
        }

        // 読み込んだリソースを開放する
        Resources.UnloadAsset(textAsset);
        textAsset = null;
        Resources.UnloadUnusedAssets();

        return true;
    }

    private TextReader CreateTextReader(string fileName)
    {
        if (this.isReadFromDisc)
            return new StreamReader(fileName);

        textAsset = Resources.Load<TextAsset>(fileName);

        return new StringReader(textAsset.text);
    }

    /// <summary>
    /// 表をクリア
    /// </summary>
    public void Clear()
    {
        Rows.Clear();
    }

    /// <summary>
    /// 表の行数を取得します。
    /// </summary>
    public int RowCount { get { return Rows.Count; } }
    /// <summary>
    /// 表の列数を取得します。
    /// </summary>
    public int ColumnCount { get { return Rows[0].Count; } }
    /// <summary>
    /// 表の1行を取得
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    public List<string> GetData(int row)
    {
        return Rows[row];
    }

    public string GetString(int row, int col)
    {
        return Rows[row][col];
    }

    public bool GetBool(int row, int col)
    {
        string data = Rows[row][col];
        return bool.Parse(data);
    }

    public int GetInt(int row, int col)
    {
        string data = Rows[row][col];
        return int.Parse(data);
    }

    public float GetFloat(int row, int col)
    {
        string data = Rows[row][col];
        return float.Parse(data);
    }

    public void SetData(string[,] data)
    {
        int height = data.GetLength(0);
        int width = data.GetLength(1);

        for (int y = 0; y < height; ++y)
        {
            Rows.Add(new List<string>());
            for (int x = 0; x < width; ++x)
                Rows[y].Add(data[x, y]);
        }
    }
}

