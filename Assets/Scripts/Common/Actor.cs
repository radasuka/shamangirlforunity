﻿using UnityEngine;
using System.Collections;

/// <summary>
/// キャラクター基底クラス
/// SpriteRendererが必要
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class Actor : MonoBehaviour
{
    /// <summary>
    /// プレハブ取得
    /// プレハブは必ず"Resources/Prefabs/"に配置すること
    /// </summary>
    /// <param name="prefab"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public static GameObject GetPrefab(GameObject prefab, string name)
    {
        return prefab ?? (prefab = Resources.Load("Prefabs/" + name) as GameObject);
    }

    /// <summary>
    /// インスタンスを生成してスクリプトを返す
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="prefab"></param>
    /// <param name="pos">位置</param>
    /// <param name="direction">方向(度数)</param>
    /// <param name="speed">速度</param>
    /// <returns></returns>
    public static T CreateInstance<T>(GameObject prefab, Vector3 pos, float direction = 0.0f, float speed = 0.0f) where T : Actor
    {
        GameObject g = Instantiate(prefab, pos, Quaternion.identity) as GameObject;
        T obj = g.GetComponent<T>();
        obj.SetVelocity(direction, speed);

        return obj;

    }

    public static T CreateInstance2<T>(GameObject prefab, float x, float y, float direction = 0f, float speed = 0f) where T : Actor
    {
        Vector3 pos = new Vector3(x, y, 0);

        return CreateInstance<T>(prefab, pos, direction, speed);
    }

    bool isExists = false;
    /// <summary>
    /// 生存フラグ
    /// </summary>
    public bool IsExists
    {
        get { return isExists; }
        set { isExists = value; }
    }

    /// アクセサ
    /// スプライトレンダラー
    private new SpriteRenderer renderer = null;
    public SpriteRenderer Renderer
    {
        get { return renderer ?? (gameObject.GetComponent<SpriteRenderer>()); }
    }
    /// <summary>
    /// 描画フラグ
    /// </summary>
    public bool Visible
    {
        get { return Renderer.enabled; }
        set { Renderer.enabled = value; }
    }
    /// <summary>
    /// ソーティングレイヤー名
    /// </summary>
    public string SortingLayer
    {
        get { return Renderer.sortingLayerName; }
        set { Renderer.sortingLayerName = value; }
    }
    /// <summary>
    /// ソーティング・オーダー
    /// </summary>
    public int SortingOrder
    {
        get { return Renderer.sortingOrder; }
        set { Renderer.sortingOrder = value; }
    }
    /// <summary>
    /// 座標(X)
    /// </summary>
    public float X
    {
        get { return transform.position.x; }
        set
        {
            Vector3 pos = transform.position;
            pos.x = value;
            transform.position = pos;
        }
    }
    /// <summary>
    /// 座標(Y)
    /// </summary>
    public float Y
    {
        get { return transform.position.y; }
        set
        {
            Vector3 pos = transform.position;
            pos.y = value;
            transform.position = pos;
        }
    }

    /// <summary>
    /// 座標を足し込む
    /// </summary>
    /// <param name="dx"></param>
    /// <param name="dy"></param>
    public void AddPosition(float dx, float dy)
    {
        X += dx;
        Y += dy;
    }

    /// <summary>
    /// 座標を設定する
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void SetPosition(float x, float y)
    {
        Vector3 pos = transform.position;
        pos.Set(x, y, 0);
        transform.position = pos;
    }

    /// <summary>
    /// スケール値(X)
    /// </summary>
    public float ScaleX
    {
        get { return transform.localScale.x; }
        set
        {
            Vector3 scale = transform.localScale;
            scale.x = value;
            transform.localScale = scale;
        }
    }
    /// <summary>
    /// スケール値(Y)
    /// </summary>
    public float ScaleY
    {
        get { return transform.localScale.y; }
        set
        {
            Vector3 scale = transform.localScale;
            scale.y = value;
            transform.localScale = scale;
        }
    }

    /// <summary>
    /// スケール値を設定
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void SetScale(float x, float y)
    {
        Vector3 scale = transform.localScale;
        scale.Set(x, y, (x + y) / 2);
        transform.localScale = scale;
    }

    /// <summary>
    /// スケール値(X/Y)
    /// </summary>
    public float Scale
    {
        get
        {
            Vector3 scale = transform.localScale;
            return (scale.x + scale.y) / 2.0f;
        }
        set
        {
            Vector3 scale = transform.localScale;
            scale.x = value;
            scale.y = value;
            transform.localScale = scale;
        }
    }

    /// <summary>
    /// スケール値を足し込む
    /// </summary>
    /// <param name="d"></param>
    public void AddScale(float d)
    {
        Vector3 scale = transform.localScale;
        scale.x += d;
        scale.y += d;
        transform.localScale = scale;
    }
    /// <summary>
    /// スケール値をかける
    /// </summary>
    /// <param name="d"></param>
    public void MulScale(float d)
    {
        transform.localScale *= d;
    }

    private new Rigidbody2D rigidbody = null;
    /// <summary>
    /// 剛体
    /// </summary>
    public Rigidbody2D Rigidbody
    {
        get { return rigidbody ?? (rigidbody = gameObject.GetComponent<Rigidbody2D>()); }
    }

    /// <summary>
    /// 移動量を設定
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="speed"></param>
    public void SetVelocity(float direction, float speed)
    {
        Vector2 v;
        v.x = Mathf.Cos(Mathf.Deg2Rad * direction) * speed;
        v.y = Mathf.Sin(Mathf.Deg2Rad * direction) * speed;
        Rigidbody.velocity = v;
    }
    /// <summary>
    /// 移動量を設定(X/Y)
    /// </summary>
    /// <param name="vx"></param>
    /// <param name="vy"></param>
    public void SetVelocityXY(float vx, float vy)
    {
        Vector2 v;
        v.x = vx;
        v.y = vy;
        Rigidbody.velocity = v;
    }
    /// <summary>
    /// 移動量をかける
    /// </summary>
    /// <param name="d"></param>
    public void MulVelocity(float d)
    {
        Rigidbody.velocity *= d;
    }

    /// <summary>
    /// 移動量(X)
    /// </summary>
    public float VX
    {
        get { return Rigidbody.velocity.x; }
        set
        {
            Vector2 v = Rigidbody.velocity;
            v.x = value;
            Rigidbody.velocity = v;
        }
    }
    /// <summary>
    /// 移動量(Y)
    /// </summary>
    public float VY
    {
        get { return Rigidbody.velocity.y; }
        set
        {
            Vector2 v = Rigidbody.velocity;
            v.y = value;
            Rigidbody.velocity = v;
        }
    }

    /// <summary>
    /// 方向(度数)
    /// </summary>
    public float Direction
    {
        get
        {
            Rigidbody2D r = GetComponent<Rigidbody2D>();
            Vector2 v = r.velocity;
            return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        }
    }
    /// <summary>
    /// 速度
    /// </summary>
    public float Speed
    {
        get
        {
            Rigidbody2D r = GetComponent<Rigidbody2D>();
            Vector2 v = r.velocity;
            return Mathf.Sqrt(v.x * v.x + v.y * v.y);
        }
    }

    /// <summary>
    /// 重力
    /// </summary>
    public float GravityScale
    {
        get { return Rigidbody.gravityScale; }
        set { Rigidbody.gravityScale = value; }
    }

    /// <summary>
    /// 回転角度
    /// </summary>
    public float Angle
    {
        get { return transform.eulerAngles.z; }
        set { transform.eulerAngles = new Vector3(0, 0, value); }
    }

    /// <summary>
    /// スプライトの設定
    /// </summary>
    /// <param name="sprite"></param>
    public void SetSprite(Sprite sprite)
    {
        Renderer.sprite = sprite;
    }

    /// <summary>
    /// 色設定
    /// </summary>
    /// <param name="r"></param>
    /// <param name="g"></param>
    /// <param name="b"></param>
    public void SetColor(float r, float g, float b)
    {
        var c = Renderer.color;
        c.r = r;
        c.g = g;
        c.b = b;
        Renderer.color = c;
    }
    /// <summary>
    /// アルファ値を設定
    /// </summary>
    /// <param name="a"></param>
    public void SetAlpha(float a)
    {
        var c = Renderer.color;
        c.a = a;
        Renderer.color = c;
    }
    /// <summary>
    /// アルファ値を取得
    /// </summary>
    /// <returns></returns>
    public float GetAlpha()
    {
        var c = Renderer.color;
        return c.a;
    }

    /// <summary>
    /// アルファ値
    /// </summary>
    public float Alpha
    {
        get { return GetAlpha(); }
        set { SetAlpha(value); }
    }

    float width = 0.0f;
    float height = 0.0f;
    /// <summary>
    /// サイズを設定
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    public void SetSize(float width, float height)
    {
        this.width = width;
        this.height = height;
    }

    /// <summary>
    /// スプライトの幅
    /// </summary>
    public float SpriteWidth
    {
        get { return Renderer.bounds.size.x; }
    }
    /// <summary>
    /// スプライトの高さ
    /// </summary>
    public float SpriteHeight
    {
        get { return Renderer.bounds.size.y; }
    }


    CircleCollider2D circleCollider = null;
    /// <summary>
    /// コリジョン（円）
    /// </summary>
    public CircleCollider2D CircleCollider
    {
        get { return circleCollider ?? (circleCollider = GetComponent<CircleCollider2D>()); }
    }
    /// <summary>
    /// 円コリジョンの半径
    /// </summary>
    public float CollisionRadius
    {
        get { return CircleCollider.radius; }
        set { CircleCollider.radius = value; }
    }
    /// <summary>
    /// 円コリジョンの有効無効を設定する
    /// </summary>
    public bool CircleColliderEnabled
    {
        get { return CircleCollider.enabled; }
        set { CircleCollider.enabled = value; }
    }

    BoxCollider2D boxCollider = null;
    /// <summary>
    /// コリジョン（矩形）
    /// </summary>
    public BoxCollider2D BoxCollider
    {
        get { return boxCollider ?? (boxCollider = GetComponent<BoxCollider2D>()); }
    }
    /// <summary>
    /// 矩形コリジョンの幅
    /// </summary>
    public float BoxColliderWidth
    {
        get { return BoxCollider.size.x; }
        set
        {
            var size = BoxCollider.size;
            size.x = value;
            BoxCollider.size = size;
        }
    }
    /// <summary>
    /// 矩形コリジョンの高さ
    /// </summary>
    public float BoxColliderHeight
    {
        get { return BoxCollider.size.y; }
        set
        {
            var size = BoxCollider.size;
            size.y = value;
            BoxCollider.size = size;
        }
    }
    /// <summary>
    /// 箱コリジョンのサイズを設定する
    /// </summary>
    /// <param name="w"></param>
    /// <param name="h"></param>
    public void SetBoxColliderSize(float w, float h)
    {
        BoxCollider.size.Set(w, h);
    }
    /// <summary>
    /// 箱コリジョンの有効無効を設定する
    /// </summary>
    public bool BoxColliderEnabled
    {
        get { return BoxCollider.enabled; }
        set { BoxCollider.enabled = value; }
    }

    /// <summary>
    /// 移動して画面内に収めるようにする
    /// </summary>
    /// <param name="v"></param>
    public void ClampScreenAndMove(Vector2 v)
    {
        Vector2 min = GetWorldMin();
        Vector2 max = GetWorldMax();
        Vector2 pos = transform.position;

        pos += v;

        // 画面内に収まるように制限をかける
        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        // 座標を反映
        transform.position = pos;
    }
    /// <summary>
    /// 画面内に収めるようにする
    /// </summary>
    public void ClampScreen()
    {
        Vector2 min = GetWorldMin();
        Vector2 max = GetWorldMax();
        Vector2 pos = transform.position;

        // 画面内に収まるように制限をかける
        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        // 座標を反映
        transform.position = pos;
    }
    /// <summary>
    /// 領域内に収めるようにする(余白加算)
    /// </summary>
    public void ClampScreenAddMargin(Vector2 margin)
    {
        //ClampScreenY();
        Vector2 pos = transform.position;
        Vector2 min = GetWorldPointToMin(margin);
        Vector2 max = GetWorldPointToMax(margin);

        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        // 座標を反映
        transform.position = pos;
    }
    /// <summary>
    /// 画面外に出たかどうか
    /// </summary>
    /// <returns></returns>
    public bool IsOutside()
    {
        Vector2 min = GetWorldMin();
        Vector2 max = GetWorldMax();
        Vector2 pos = transform.position;

        if (pos.x < min.x || pos.y < min.y)
            return true;
        if (pos.x > max.x || pos.y > max.y)
            return true;

        return false;
    }

    /// <summary>
    /// 画面の左下のワールド座標を取得する
    /// </summary>
    /// <returns></returns>
    public Vector2 GetWorldMin(bool noMergin = false)
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(Vector2.zero);

        if (noMergin)
            // そのまま返す
            return min;

        // 自身のサイズを考慮する
        min.x += this.width;
        min.y += this.height;

        return min;
    }
    /// <summary>
    /// 画面右上のワールド座標を取得する
    /// </summary>
    /// <param name="noMargin"></param>
    /// <returns></returns>
    public Vector2 GetWorldMax(bool noMargin = false)
    {
        Vector2 max = Camera.main.ViewportToWorldPoint(Vector2.one);

        if (noMargin)
            // そのまま返す
            return max;

        // 自身のサイズを考慮する
        max.x -= this.width;
        max.y -= this.height;

        return max;
    }
    /// <summary>
    /// スクリーン座標からワールド座標を取得
    /// </summary>
    /// <param name="point"></param>
    /// <param name="noMargin"></param>
    /// <returns></returns>
    public Vector2 GetWorldPointToMin(Vector2 point, bool noMargin = false)
    {
        Vector2 min = Camera.main.ScreenToWorldPoint(new Vector3(point.x, point.y));

        if (noMargin)
            // そのまま返す
            return min;

        // 自身のサイズを考慮する
        min.x += this.width;
        min.y += this.height;

        return min;
    }
    /// <summary>
    /// スクリーン座標からワールド座標を取得
    /// </summary>
    /// <param name="point"></param>
    /// <param name="noMargin"></param>
    /// <returns></returns>
    public Vector2 GetWorldPointToMax(Vector2 point, bool noMargin = false)
    {
        Vector2 max = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width - point.x, Screen.height - point.y));

        if (noMargin)
            // そのまま返す
            return max;

        // 自身のサイズを考慮する
        max.x -= this.width;
        max.y -= this.height;

        return max;
    }

    /// <summary>
    /// 消滅（メモリから削除）
    /// </summary>
    public void DestroyObj()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// アクティブにする
    /// </summary>
    public virtual void Revive()
    {
        gameObject.SetActive(true);
        IsExists = true;
        Visible = true;
    }
    /// <summary>
    /// 消滅する（オーバーライド可能）
    /// ただし base.Vanish()を呼ばないと消滅しなくなることに注意
    /// </summary>
    public virtual void Vanish()
    {
        VanishCannotOverride();
    }
    /// 消滅する（オーバーライド禁止）
    public void VanishCannotOverride()
    {
        gameObject.SetActive(false);
        IsExists = false;
    }

}
