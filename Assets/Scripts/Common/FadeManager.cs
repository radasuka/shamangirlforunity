﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// シーン遷移時のフェードイン・アウトを制御するためのクラス
/// </summary>
public class FadeManager : MonoBehaviour
{
    // シングルトンインスタンス
    private static FadeManager singletonInstance;

    /// <summary>
    /// 単一インスタンスを取得します
    /// </summary>
    public static FadeManager Instance
    {
        get
        {
            // 初めて起動された場合
            if (singletonInstance == null)
            {
                // インスタンスを生成
                singletonInstance = FindObjectOfType(typeof(FadeManager)) as FadeManager;
                // インスタンスを生成することに失敗した場合
                if (singletonInstance == null)
                {
                    Debug.LogError(typeof(FadeManager) + "is nothing");
                }
            }

            return singletonInstance;
        }
    }

    //フェード中の透明度
    private float fadeAlpha = 0;
    //フェード中かどうか
    private bool isFading = false;
    //フェード色
    public Color FadeColor;

    private Rect rect = new Rect();

    //初期処理
    public void Awake()
    {
        if (this != Instance)
        {
            Destroy(this.gameObject);
            return;
        }
        // このゲームオブジェクトはシーンが切り替わっても削除しない
        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// 画面描画
    /// </summary>
    public void OnGUI()
    {
        // Fade中の描画処理
        if (this.isFading)
        {
            //色と透明度を更新して白テクスチャを描画 .
            this.FadeColor.a = this.fadeAlpha;
            GUI.color = this.FadeColor;
            //GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Texture2D.whiteTexture);
            GUI.DrawTexture(rect, Texture2D.whiteTexture);
        }

    }

    /// <summary>
    /// 画面遷移
    /// </summary>
    /// <param name="scene">シーン</param>
    /// <param name="interval">名暗転にかかる時間</param>
    /// <param name="color"></param>
    public void LoadLevel(string scene, float interval, Color color, Rect rect)
    {
        this.rect = rect;
        //コルーチン開始
        StartCoroutine(TransScene(scene, interval, color));
    }

    /// <summary>
    /// シーン遷移用コルーチン
    /// </summary>
    /// <param name="scene">シーン</param>
    /// <param name="interval">名暗転にかかる時間</param>
    /// <param name="color"></param>
    /// <returns></returns>
    private IEnumerator TransScene(string scene, float interval, Color color)
    {
        //フェードする色を指定された色に変更する
        FadeColor = color;

        //だんだん暗く
        this.isFading = true;
        float time = 0;
        while (time <= interval)
        {
            this.fadeAlpha = Mathf.Lerp(0f, 1f, time / interval);
            time += Time.deltaTime;
            yield return 0;
        }

        //シーン切替
        SceneManager.LoadScene(scene);

        //だんだん明るく
        time = 0;
        while (time <= interval)
        {
            this.fadeAlpha = Mathf.Lerp(1f, 0f, time / interval);
            time += Time.deltaTime;
            yield return 0;
        }

        this.isFading = false;
    }

    public void NextStage(float interval, Color color, Rect rect)
    {
        this.rect = rect;
        //コルーチン開始
        StartCoroutine(TransStage(interval, color));
    }

    private IEnumerator TransStage(float interval, Color color)
    {
        //フェードする色を指定された色に変更する
        FadeColor = color;

        //だんだん暗く
        this.isFading = true;
        float time = 0;
        while (time <= interval)
        {
            this.fadeAlpha = Mathf.Lerp(0f, 1f, time / interval);
            time += Time.deltaTime;
            yield return 0;
        }

        //シーン切替
        yield return new WaitForSeconds(1f);

        //だんだん明るく
        time = 0;
        while (time <= interval)
        {
            this.fadeAlpha = Mathf.Lerp(1f, 0f, time / interval);
            time += Time.deltaTime;
            yield return 0;
        }

        this.isFading = false;

    }
}

