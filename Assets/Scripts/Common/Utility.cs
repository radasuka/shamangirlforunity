﻿using System;
using UnityEngine;

public class Utility
{
    /// <summary>
    /// Mathf.Cosの角度指定版
    /// </summary>
    /// <param name="Deg"></param>
    /// <returns></returns>
    public static float CosEx(float Deg)
    {
        return Mathf.Cos(Mathf.Deg2Rad * Deg);
    }
    /// <summary>
    /// Mathf.Sinの角度指定版
    /// </summary>
    /// <param name="Deg"></param>
    /// <returns></returns>
    public static float SinEx(float Deg)
    {
        return Mathf.Sin(Mathf.Deg2Rad * Deg);
    }
    /// <summary>
    /// -value から value未満の乱数を返す
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static float Random(float value)
    {
        return UnityEngine.Random.Range(-value, value);
    }

    /// <summary>
    /// 入力方向を取得する
    /// </summary>
    /// <returns></returns>
    public static Vector2 GetInputVector()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        return new Vector2(x, y).normalized;
    }

    /// <summary>
    /// トークンを動的生成する
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="SpriteFile"></param>
    /// <param name="SpriteName"></param>
    /// <param name="objName"></param>
    /// <returns></returns>
    public static Actor CreateActor(float x, float y, string spriteFile, string spriteName, string objName = "Actor")
    {
        GameObject obj = new GameObject(objName);
        obj.AddComponent<SpriteRenderer>();
        obj.AddComponent<Rigidbody2D>();
        obj.AddComponent<Actor>();

        Actor actor = obj.GetComponent<Actor>();

        // スプライト設定
        actor.SetSprite(GetSprite(spriteFile, spriteName));

        // 座標を設定
        actor.X = x;
        actor.Y = y;

        // 重力を無効にする
        actor.GravityScale = 0;

        return actor;
    }

    /// <summary>
    /// スプライトをリソースから取得する.
    /// ※スプライトは「Resources/Sprites」以下に配置していなければなりません.
    /// ※fileNameに空文字（""）を指定するとシングルスプライトから取得します.
    /// </summary>
    /// <param name="spriteFile"></param>
    /// <param name="spriteName"></param>
    /// <returns></returns>
    public static Sprite GetSprite(string fileName, string spriteName)
    {
        if (spriteName == "")
            // シングルスプライト
            return Resources.Load<Sprite>(fileName);
        else
        {
            // マルチスプライト
            Sprite[] sprites = Resources.LoadAll<Sprite>(fileName);
            return Array.Find<Sprite>(sprites, (sprite) => sprite.name.Equals(fileName));
        }
    }

    private static Rect guiRect = new Rect();
    private static Rect GetGUIRect()
    {
        return guiRect;
    }

    private static GUIStyle guiStyle = null;
    private static GUIStyle GetGUIStyle()
    {
        return guiStyle ?? (guiStyle = new GUIStyle());
    }

    /// <summary>
    /// フォントサイズを設定
    /// </summary>
    /// <param name="size"></param>
    public static void SetFontSize(int size)
    {
        GetGUIStyle().fontSize = size;
    }
    /// <summary>
    /// フォントカラーを設定
    /// </summary>
    /// <param name="color"></param>
    public static void SetFontColor(Color color)
    {
        GetGUIStyle().normal.textColor = color;
    }
    /// <summary>
    /// フォント位置設定
    /// </summary>
    /// <param name="align"></param>
    public static void SetFontAlignment(TextAnchor align)
    {
        GetGUIStyle().alignment = align;
    }

    /// <summary>
    /// ラベルの描画
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="w"></param>
    /// <param name="h"></param>
    /// <param name="text"></param>
    public static void GUILabel(float x, float y, float w, float h, string text)
    {
        Rect rect = GetGUIRect();
        rect.x = x;
        rect.y = y;
        rect.width = w;
        rect.height = h;

        GUI.Label(rect, text, GetGUIStyle());
    }

    /// <summary>
    /// ボタンの配置
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="w"></param>
    /// <param name="h"></param>
    /// <param name="text"></param>
    /// <returns></returns>
    public static bool GUIButton(float x, float y, float w, float h, string text)
    {
        Rect rect = GetGUIRect();
        rect.x = x;
        rect.y = y;
        rect.width = w;
        rect.height = h;

        return GUI.Button(rect, text, GetGUIStyle());
    }
}

