﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Actor管理クラス
/// </summary>
public class ActorManager<T> where T : Actor
{
    // 保持するオブジェクトの数
    int poolListLength = 0;
    // プレハブ
    GameObject prefab = null;
    // 保持しているオブジェクトのリスト
    List<T> pool = null;
    // Order in Layer
    int order = 0;

    // 固定アロケーションであるか
    private bool IsFixedLenght { get { return poolListLength > 0; } }

    /// <summary>
    /// ForEach関数に渡す関数の型
    /// </summary>
    /// <param name="t"></param>
    public delegate void FuncT(T t);

    /// <summary>
    /// コンストラクタ
    /// プレハブは必ず"Resources/Prefabs/"に配置すること
    /// </summary>
    /// <param name="prefabName">プレハブ名</param>
    /// <param name="size">確保するサイズ</param>
    public ActorManager(string prefabName, int size = 0)
    {
        this.poolListLength = size;
        this.prefab = Resources.Load("Prefabs/" + prefabName) as GameObject;

        if (prefab == null)
            Debug.LogError("prefabがありません. name=" + prefabName);

        pool = new List<T>();

        if(IsFixedLenght)
        {
            // サイズ指定があれば固定アロケーションとする
            for (int index = 0; index < size; ++index)
            {
                GameObject g = GameObject.Instantiate(prefab, new Vector3(), Quaternion.identity) as GameObject;
                T obj = g.GetComponent<T>();
                obj.VanishCannotOverride();
                pool.Add(obj);
            }
        }
    }

    /// <summary>
    /// オブジェクトを再利用する
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="x">位置(x)</param>
    /// <param name="y">位置(y)</param>
    /// <param name="direction">方向(度数)</param>
    /// <param name="speed">速度</param>
    /// <returns></returns>
    private T Recycle(T obj, float x, float y, float direction, float speed)
    {
        // 復活
        obj.Revive();
        obj.SetPosition(x, y);

        if (obj.Rigidbody != null)
            // Rigidbody2Dがあるときのみ速度を設定する
            obj.SetVelocity(direction, speed);

        obj.Angle = 0;
        // Order in Layerをインクリメントして設定する
        obj.SortingOrder = this.order;
        order++;

        return obj;
    }

    /// <summary>
    /// インスタンスを取得する
    /// </summary>
    /// <param name="x">位置(x)</param>
    /// <param name="y">位置(y)</param>
    /// <param name="direction">方向(度数)</param>
    /// <param name="speed">速度</param>
    /// <returns></returns>
    public T Add(float x, float y, float direction = 0f, float speed = 0f)
    {
        foreach(T obj in pool)
        {
            if (obj.IsExists == false)
                // 未使用のオブジェクトを見つけた
                return Recycle(obj, x, y, direction, speed);
        }

        if(poolListLength <= pool.Count)
        {
            // 自動で拡張
            GameObject g = GameObject.Instantiate(this.prefab, new Vector3(), Quaternion.identity) as GameObject;
            T obj = g.GetComponent<T>();
            pool.Add(obj);

            return Recycle(obj, x, y, direction, speed);
        }

        return null;
    }

    /// <summary>
    /// すべてのインスタンスにラムダ式を実行する
    /// </summary>
    /// <param name="func"></param>
    public void ForEach(FuncT func)
    {
        foreach (var obj in pool)
            func(obj);
    }

    /// <summary>
    /// 生存しているインスタンスを取得
    /// </summary>
    public IEnumerable<T> GetExistObjects
    {
        get { return pool.Where(x => x.IsExists); }
    }

    /// <summary>
    /// 生存するインスタンスに対してラムダ式を実行する
    /// </summary>
    /// <param name="func"></param>
    public void ForEachExist(FuncT func)
    {
        foreach(var obj in pool)
        {
            if (obj.IsExists)
                func(obj);
        }
    }

    /// <summary>
    /// 生存しているインスタンスをすべて破棄する
    /// </summary>
    public void Vanish()
    {
        ForEachExist(t => t.Vanish());
    }

    /// <summary>
    /// インスタンスの生存数を取得する
    /// </summary>
    /// <returns></returns>
    public int Count()
    {
        return pool.Count(x => x.IsExists);
    }

}

