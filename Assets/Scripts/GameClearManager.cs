﻿using UnityEngine;
using System.Collections;

public class GameClearManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        // サウンドロード
        Sound.LoadSe("GameClear", "SE/GameClear");
        Sound.PlaySe("GameClear");
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
            FadeManager.Instance.LoadLevel("TitleScene", 1f, Color.black, new Rect(0, 0, Screen.width, Screen.height));
    }
}
