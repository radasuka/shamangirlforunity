﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeText : MonoBehaviour
{
    //フェード中の透明度
    private float fadeAlpha = 0;
    // Text
    private Text text;
    // 色
    private Color color;

    public bool isInOut = true;

    // Use this for initialization
    void Start()
    {
        // Textを取得
        text = GetComponent<Text>();
        // 初期カラーを設定
        color = text.color;
        color.a = 0;
        text.color = color;

        //  フェードイン開始
        if (isInOut)
            StartCoroutine(FadeInOut());
        else
            StartCoroutine(FadeIn());

    }

    // フェードイン・アウト
    private IEnumerator FadeInOut()
    {
        float time = 0;
        while (time <= 1)
        {
            this.fadeAlpha = Mathf.Lerp(0f, 1f, time / 1);
            time += Time.deltaTime;
            yield return 0;
        }

        yield return new WaitForSeconds(1);

        time = 0;
        while (time <= 1)
        {
            this.fadeAlpha = Mathf.Lerp(1f, 0f, time / 1);
            time += Time.deltaTime;
            yield return 0;
        }

    }

    private IEnumerator FadeIn()
    {
        float time = 0;
        while (time <= 1)
        {
            this.fadeAlpha = Mathf.Lerp(0f, 1f, time / 1);
            time += Time.deltaTime;
            yield return 0;
        }
    }

    private void OnGUI()
    {
        color.a = fadeAlpha;
        text.color = color;
    }

}
