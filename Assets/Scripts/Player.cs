﻿using UnityEngine;
using System.Collections;

public class Player : Actor
{
    // プレハブ
    private static GameObject prefab = null;
    // 無敵時間
    private const float INVINCIBLE_TIME = 2.5f;

    // プレイヤーの状態
    public enum ModeType
    {
        ModeType_Default,       // 通常
        ModeType_Bomb,          // ボム発射中
        ModeType_Invincible,    // 無敵(敵機弾に当たらなくなる)
        ModeType_Init
    }
    // Sprite
    public Sprite rightSprite;
    public Sprite leftSprite;
    public Sprite standSprite;
    // 移動速度
    public float MoveSpeed = 10;
    // 弾の発射間隔
    public float ShotDelay;
    // 弾の速度
    public float BulletSpeed = 20f;
    // 無敵解除まで計測する時間
    public float InvincibleTIme;
    // mode 遷移後からの累積時間
    public float ModeTime { get; private set; }
    // 残機数
    public int LifeCount { get; private set; }

    // 状態変数
    private ModeType mode;
    // 状態変数を取得または設定
    public ModeType Mode
    {
        get { return mode; }
        set
        {
            if (mode == value)
                return;
            mode = value;
            ModeTime = 0;
        }
    }

    public bool DebugInvincible = false;

    /// <summary>
    /// インスタンス生成
    /// </summary>
    /// <param name="x">位置(x)</param>
    /// <param name="y">位置(y)</param>
    /// <param name="direction">方向(度数)</param>
    /// <param name="speed">速度</param>
    /// <returns></returns>
    public static Player Add(float x, float y, float direction, float speed)
    {
        // プレハブ取得
        prefab = GetPrefab(prefab, "Player");
        return CreateInstance2<Player>(prefab, x, y, direction, speed);
    }

    /// <summary>
    /// 初期化
    /// </summary>
    void Start()
    {
        IsExists = true;
        var w = SpriteWidth / 2;
        var h = SpriteHeight / 2;
        SetSize(w, h);
        LifeCount = 3;
        Mode = ModeType.ModeType_Init;
    }

    /// <summary>
    /// 更新処理
    /// </summary>
    void Update()
    {
        ModeTime += Time.deltaTime;

        switch (Mode)
        {
            case ModeType.ModeType_Default:
                // 領域内に収める
                ClampScreenAddMargin(new Vector2(370, 0));
                // 入力処理
                HandleInput();
                break;

            case ModeType.ModeType_Invincible:
                //RestartStage();
                SetVelocity(90, 2);
                Renderer.enabled = !Renderer.enabled;
                if (ModeTime > INVINCIBLE_TIME || (Y >= -4 && Input.anyKey))
                {
                    Renderer.enabled = true;
                    Mode = ModeType.ModeType_Default;
                }
                break;

            case ModeType.ModeType_Bomb:
                break;

            case ModeType.ModeType_Init:
                break;
        }
    }

    /// <summary>
    /// 入力処理
    /// </summary>
    private void HandleInput()
    {
        // 入力方向を取得
        Vector2 vec = Utility.GetInputVector();

        // 低速モード
        if (Input.GetKey(KeyCode.LeftShift))
            MoveSpeed = 2.5f;
        else
            MoveSpeed = 10f;

        // 移動
        SetVelocityXY(vec.x * MoveSpeed, vec.y * MoveSpeed);

        // 攻撃
        if (Input.GetKey(KeyCode.Z))
            StartCoroutine(Shot());

        // 左右の動きによってSpriteを変更
        if (VX >= 1)
            SetSprite(rightSprite);
        else if (VX <= -1)
            SetSprite(leftSprite);
        else
            SetSprite(standSprite);

        if (Input.GetKeyDown(KeyCode.I))
        {
            DebugInvincible = !DebugInvincible;
            Debug.Log("DebugInvincible" + DebugInvincible);
        }
    }

    private new void ClampScreen()
    {
        //ClampScreenY();
        Vector2 pos = transform.position;
        Vector2 min = Camera.main.ScreenToWorldPoint(new Vector3(370, 0));
        Vector2 max = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width - 370, Screen.height));

        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        // 座標を反映
        transform.position = pos;
    }

    private IEnumerator Shot()
    {
        //if (isRunning)
        //    yield break;

        //isRunning = true;

        //Bullet.Add(X, Y, 90, BulletSpeed);
        Bullet.Add(X + 0.2f, Y + 0.3f, 90, BulletSpeed);
        Bullet.Add(X, Y + 0.3f, 90, BulletSpeed);
        Bullet.Add(X - 0.2f, Y + 0.3f, 90, BulletSpeed);
        Sound.PlaySe("PlayerShot");

        yield return new WaitForSeconds(ShotDelay);

        //isRunning = false;
    }

    // ぶつかった瞬間に呼び出される
    private void OnTriggerEnter2D(Collider2D collider)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(collider.gameObject.layer);

        if (layerName == "Bullet(Enemy)")
        {
            // 弾の削除
            EnemyBullet eb = collider.GetComponent<EnemyBullet>();
            eb.Vanish();
        }
        // レイヤー名がBullet (Enemy)またはEnemyの場合は爆発
        if (layerName == "Bullet(Enemy)" || layerName == "Enemy")
        {
            if (DebugInvincible == false)
                // 消滅
                if (Mode == ModeType.ModeType_Default)
                {
                    Mode = ModeType.ModeType_Invincible;
                    --LifeCount;
                    Vanish();

                    // 消滅エフェクト
                    for (int i = 0; i < 8; ++i)
                    {
                        Particle p = Particle.Add(X, Y);
                        p.SetColor(1, 1, 1);
                    }
                    EnemyBullet.Manager.Vanish();

                    Sound.PlaySe("PlayerHit");
                }
        }
    }
}
