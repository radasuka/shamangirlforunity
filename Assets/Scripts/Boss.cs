﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : Actor
{
    // プレハブ
    private static GameObject prefab = null;
    // 追尾弾のターゲット
    public static Player Target = null;
    // ボスの基本位置
    private readonly Vector2 basePosition = new Vector2(0, 3f);

    // コルーチンの動作状態
    private bool isRunning;

    // 各弾幕のHP
    private int[] bulletHps = { 100, 200, 300, 400, 500, 600 };

    // Sprite
    public Sprite[] Sprites = new Sprite[2];

    // 出現時間
    public float[] SpawnTime { get; private set; }
    // 移動パターン
    public int Pattern;
    // 敵の種類
    public int TypeId;
    // 弾幕発射時間
    public float ShotTime;
    // 弾幕終了までの時間
    public float ShotEndTime;
    // 弾幕の種類
    public int ShotType;
    // 弾幕数
    public List<int> ShotNums = new List<int>();
    // 現在の体力
    public int CurrentHP { get; private set; }
    // 最大HP
    public int MaxHP { get; private set; }
    // 各弾幕のHP
    public List<int> BulletHPs { get; private set; }
    // 弾の種類
    public int BulletType;
    // 待機時間
    public float WaitTime;
    // ポイント
    public int Point;
    // アクティブになってからの経過時間
    public float ActiveTime;

    private delegate IEnumerator EnemyFunc();
    // 移動パターン
    private List<EnemyFunc> enemyPatterns = new List<EnemyFunc>();
    // 弾幕パターン
    private List<EnemyFunc> shots = new List<EnemyFunc>();

    private Coroutine coroutine;

    // 中ボスかどうか
    public bool IsMedium;

    private Slider hpBar;

    public bool isDead;
    /// <summary>
    /// 状態
    /// </summary>
    public enum ModeType
    {
        ModeType_Init,          // 初期化
        ModeType_Shot,          // 攻撃中
        ModeType_Waite,         // 待機中
        ModeType_Invincible,    // 無敵(弾に当たらなくなる)
    }
    private ModeType mode;
    /// <summary>
    /// 状態変数
    /// </summary>
    public ModeType Mode
    {
        get { return mode; }
        set
        {
            if (mode == value)
                return;
            mode = value;
            ModeTime = 0;
        }
    }

    /// <summary>
    /// mode 遷移後からの累積時間
    /// </summary>
    public float ModeTime { get; private set; }

    /// <summary>
    /// インスタンス生成
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="direction"></param>
    /// <param name="speed"></param>
    /// <returns></returns>
    public static Boss Add(float x, float y, float direction, float speed, int currentStageNum, Slider hpBar)
    {
        // インスタンス取得
        prefab = GetPrefab(prefab, "Enemy/Boss");
        Boss obj = CreateInstance2<Boss>(prefab, x, y, direction, speed);

        obj.Mode = ModeType.ModeType_Init;

        //--------TODO:CSV読み込み化--------------
        // ボス登場時間
        obj.SpawnTime = new float[2];
        obj.SpawnTime[0] = 12f;
        obj.SpawnTime[1] = 35f;
        // ポイントを設定
        obj.Point = 1000;
        //---------------------------------------

        // ボスのタイプを設定
        obj.TypeId = currentStageNum - 1;
        // スプライトを設定
        obj.SetSprite(obj.Sprites[obj.TypeId]);

        // 弾幕登録
        obj.shots.Add(obj.Shot0);
        obj.shots.Add(obj.Shot1);
        obj.shots.Add(obj.Shot2);
        obj.shots.Add(obj.Shot3);
        obj.shots.Add(obj.Shot4);
        obj.shots.Add(obj.Shot5);

        // 弾幕数を設定
        obj.ShotNums.Add(1);
        obj.ShotNums.Add(5);

        obj.BulletHPs = new List<int>();
        // 各弾幕の体力を設定
        foreach (var hps in obj.bulletHps)
            obj.BulletHPs.Add(hps);

        obj.hpBar = hpBar;

        obj.IsMedium = true;
        obj.InitBoss();

        // 一度消滅させる
        obj.VanishCannotOverride();

        return obj;
    }

    // ボス初期化
    public void InitBoss()
    {
        if (IsMedium)
        {
            EnemyBullet.Manager.Vanish();
            //Enemy.Manager.Vanish();

            ShotType = -1;
        }

        isDead = false;
        Mode = ModeType.ModeType_Waite;
        ShotEndTime = 99 * 60;
        ShotType++;
        WaitTime = 0;
        BaseMove(1);
    }

    // 弾幕発射準備
    private void InitShot()
    {
        Mode = ModeType.ModeType_Shot;
        WaitTime = 0;
        CurrentHP = BulletHPs[ShotType];
        MaxHP = CurrentHP;

        hpBar.maxValue = MaxHP;

        // 弾幕発射
        coroutine = StartCoroutine(shots[ShotType]());
    }

    private void Start()
    { }

    // 更新処理
    private void Update()
    {
        hpBar.value = CurrentHP;
        ActiveTime += Time.deltaTime;
        ModeTime += Time.deltaTime;

        switch (Mode)
        {
            case ModeType.ModeType_Init:
                break;

            case ModeType.ModeType_Shot:
                ShotEndTime -= Time.deltaTime;
                ShotTime += Time.deltaTime;

                // 弾幕中で体力が無くなったら
                if (CurrentHP <= 0 || ShotEndTime <= 0)
                {
                    CurrentHP = 0;
                    // 定位置まで移動させる
                    BaseMove(2f);
                    // 弾幕を消滅させる
                    EnemyBullet.Manager.Vanish();

                    // 弾幕を停止
                    StopCoroutine(coroutine);
                    isRunning = false;

                    if (IsMedium)
                        IsMedium = false;

                    // 弾幕が全て終了したかどうか
                    if (ShotType == ShotNums[0] || ShotType == ShotNums[1])
                    {
                        Score.Instance.AddScore(Point);

                        if (ShotType == ShotNums[0])
                            Vanish();
                        else
                        {
                            DestroyObj();
                            isDead = true;
                        }

                        for (int i = 0; i < 10; ++i)
                            Particle.Add(X, Y);

                        return;
                    }
                    else
                        InitBoss();
                }
                break;

            case ModeType.ModeType_Waite:
                Wait();
                break;
        }
    }

    private IEnumerator Slow()
    {
        if (isRunning)
            yield break;
        isRunning = true;

        Time.timeScale = 0.5f;

        yield return new WaitForSeconds(1f);

        Time.timeScale = 1;

        isRunning = false;
    }

    // ボスを delay秒 かけて定位置まで移動させる
    private void BaseMove(float delay)
    {
        iTween.MoveTo(gameObject, basePosition, delay);
    }

    // 指定した範囲内でランダムに移動させる
    private void MoveBossPos(float x1, float y1, float x2, float y2, float dist, float t)
    {
        for (int i = 0; i < 1000; i++)
        {
            //今のボスの位置をセット
            float x = X;
            float y = Y;
            float dir;
            //適当に向かう方向を決める
            dir = Random.Range(-Mathf.PI, Mathf.PI);

            //そちらに移動させる
            x += Mathf.Cos(dir) * dist;
            y += Mathf.Sin(dir) * dist;

            //その点が移動可能範囲なら
            if (x1 <= x && x <= x2 && y1 <= y && y <= y2)
            {
                iTween.MoveTo(gameObject, new Vector3(x, y), t);
                return;
            }
        }
    }

    // 待機させる
    private void Wait()
    {
        float t = 0.5f;

        WaitTime += Time.deltaTime;

        // 0.5秒待機したら弾幕セット
        if (WaitTime > t)
            InitShot();
    }

    #region 弾幕系
    // 狙い撃つ角度を取得
    public float GetAim()
    {
        float dx = Target.X - X;
        float dy = Target.Y - Y;

        return Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;
    }
    // 狙い撃つ角度を取得(ラジアン)
    public float GetAimRad()
    {
        float dx = Target.X - X;
        float dy = Target.Y - Y;

        return Mathf.Atan2(dy, dx);
    }

    // 円形弾
    private IEnumerator Shot0()
    {
        if (isRunning)
            yield break;
        isRunning = true;

        yield return new WaitForSeconds(1);
        while (ShotEndTime >= 0)
        {
            yield return new WaitForSeconds(0.5f);
            float dir = GetAim();
            int n = 20;
            for (int i = 0; i < n; ++i)
            {
                dir += (Mathf.PI * 2 / n) * Mathf.Rad2Deg;
                EnemyBullet.Add(0, X, Y, dir, 3, 0, 0);
                ShotTime = 0;
            }
            Sound.PlaySe("EnemyShot");
            Debug.Log(ShotType);
        }
        isRunning = false;
    }
    // サイレントセレナ
    private IEnumerator Shot1()
    {
        if (isRunning)
            yield break;
        isRunning = true;

        float t = 0;
        int cnum = 0;
        float dir = 0;
        float baseDir = 0;

        cnum = 0;
        while (ShotEndTime >= 0)
        {
            // 1周期の最初なら
            if (t == 0)
            {
                baseDir = GetAim();

                // 4回中4回目なら
                if (cnum % 4 == 3)
                    MoveBossPos(-2, 2, 2, 3, 1, 1);
            }

            // 周期の半分で角度を変える
            if (t == 60 / 2 - 1)
                //if (t >= 0.5f)
                baseDir += (Mathf.PI * 2) / 20 / 2;

            //円形
            //10カウントに1回ずつ
            if (t % (60 / 10) == 0)
            {
                //ボスと自機との成す角
                dir = GetAim();

                // PI2/20度ずつ1周
                for (int i = 0; i < 20; ++i)
                {
                    dir = (baseDir + ((Mathf.PI * 2) / 20 * i)) * Mathf.Rad2Deg;
                    EnemyBullet eb = EnemyBullet.Add(14, X, Y, dir, 2.7f, 0, 0);
                    eb.Angle = dir + 90;
                }
            }
            //ランダム直下落下
            if (t % 4 == 0)
            {
                float x = UnityEngine.Random.Range(-3.5f, 3.5f);
                float y = UnityEngine.Random.Range(2f, 4f);
                float speed = 1.5f + UnityEngine.Random.Range(-0.5f, 0.5f);
                EnemyBullet.Add(13, x, y, 270, speed, 0, 0);
                ShotTime = 0;
            }

            t++;
            if (t == 60 - 1)
            {
                t = 0f;
            }
            cnum++;

            yield return new WaitForEndOfFrame();
        }
        isRunning = false;
    }
    // パーフェクトフリーズ
    private IEnumerator Shot2()
    {
        if (isRunning)
            yield break;
        isRunning = true;

        int t = 0;
        int bt = 0;
        float dir = 0;
        float speed = 0;
        float pi2 = Mathf.PI * 2;

        while (ShotEndTime >= 0)
        {

            if (t == 0 || t == 120)
                MoveBossPos(-1.5f, 2.5f, 1.5f, 3.5f, 1, 1.2f);

            // 最初のランダム発射
            if (t < 180)
            {
                // 1カウントに2回発射
                for (int i = 0; i < 2; ++i)
                {
                    // 7種類の色をランダムに
                    int id = Random.Range(3, 10);
                    dir = (Random.Range(-(pi2 / 20), pi2 / 20) + pi2 / 10 * t) * Mathf.Rad2Deg;
                    speed = 2.2f + Random.Range(-2.0f, 2.0f);

                    EnemyBullet.Add(id, X, Y, dir, speed, 0, 0);
                }
            }

            // 自機依存による8方向発射
            if (210 < t && t < 270 && t % 3 == 0)
            {
                dir = GetAimRad();
                for (int i = 0; i < 8; ++i)
                {
                    // 自機とボスとの成す角を基準に８方向に発射する
                    float dir2 = dir - Mathf.PI / 2 * 0.8f + Mathf.PI * 0.8f / 7 * i + Utility.Random(Mathf.PI / 180);
                    dir2 = dir2 * Mathf.Rad2Deg;

                    speed = 2.5f + Utility.Random(0.3f);

                    EnemyBullet.Add(3, X, Y, dir2, speed, 0, 2);
                }
            }

            var it = EnemyBullet.Manager.GetExistObjects;
            foreach (var eb in it)
            {
                // tが190の時に全てストップさせ、白くし、カウントリセット
                if (eb.state == 0)
                {
                    if (t == 190)
                    {
                        speed = 0;
                        eb.SetVelocity(0, 0);
                        eb.SetSprite(eb.sprites[12]);
                        eb.state = 1;
                        eb.ActiveTime = 0;
                        bt = 0;
                    }
                }
                // ランダムな方向に移動し始める
                if (eb.state == 1)
                {
                    if (bt == 200)
                    {
                        dir = Random.Range(-Mathf.PI, Mathf.PI) * Mathf.Rad2Deg;
                        eb.SetVelocity(dir, 0.1f);
                    }
                    if (bt > 200)
                    {
                        dir = eb.Direction;
                        speed = eb.Speed;
                        speed += 0.01f;
                        eb.Angle += 1f;
                        eb.SetVelocity(dir, speed);
                    }

                }
            }
            bt++;
            t++;
            if (t == 650)
            {
                t = 0;
            }

            yield return new WaitForEndOfFrame();
        }
        isRunning = false;
    }
    // 恋の迷路
    private IEnumerator Shot3()
    {
        if (isRunning)
            yield break;
        isRunning = true;

        const int DF003 = 20;
        int t = 0;
        int t2 = 0;
        float dir = 0;
        float[] baseDir = new float[2];
        int tcnt = 0;
        int cnt = 0;
        int cnum = 0;
        float pi2 = Mathf.PI * 2;

        while (ShotEndTime >= 0)
        {
            // 最初なら
            if (t2 == 0)
            {
                // 中心に移動
                iTween.MoveTo(gameObject, Vector2.zero, 0.9f);
                cnum = 0;
            }

            // 1周期の最初なら
            if (t == 0)
            {
                // 自機狙い
                baseDir[0] = GetAimRad();
                cnt = 0;
                tcnt = 2;
            }

            if (t < 540 && t % 3 == 0)
            {
                dir = GetAimRad();
                //撃たない方向なら撃たない
                if (tcnt - 2 == cnt || tcnt - 1 == cnt)
                {
                    if (tcnt - 1 == cnt)
                    {
                        // ベースとなる角度をセット
                        baseDir[1] = baseDir[0] + pi2 / DF003 * cnt * (cnum % 2 == 0 ? -1 : 1) - pi2 / (DF003 * 6) * 3;
                        tcnt += DF003 - 2;
                    }
                }
                else
                {
                    //１回に6発ずつうつ
                    for (int i = 0; i < 6; ++i)
                    {
                        int id = cnum % 2 == 0 ? 14 : 17;
                        float localDir = baseDir[0] + pi2 / DF003 * cnt * (cnum % 2 == 0 ? -1 : 1) + pi2 / (DF003 * 6) * i * (cnum == 0 ? -1 : 1);

                        EnemyBullet e = EnemyBullet.Add(id, X, Y, localDir * Mathf.Rad2Deg, 2, 0, 0);
                        e.Angle = localDir * Mathf.Rad2Deg + 90;
                    }
                }
                cnt++;
            }

            // 少し大きな弾で円形発射
            if (40 < t && t < 540 && t % 30 == 0)
            {
                for (int j = 0; j < 3; ++j)
                {
                    dir = baseDir[1] - pi2 / 36 * 4;
                    for (int i = 0; i < 27; ++i)
                    {
                        int id = cnum % 2 == 0 ? 3 : 9;
                        float sp = 4 - 1.6f / 3 * j;

                        EnemyBullet.Add(id, X, Y, dir * Mathf.Rad2Deg, sp, 0, 0);
                        dir -= pi2 / 36;
                    }
                }
            }

            if (t == 600 - 1)
            {
                cnum++;
                t = 0;
            }

            t++;
            t2++;
            yield return new WaitForEndOfFrame();
        }
        isRunning = false;
    }
    // ケロちゃん風雨にも負けず
    private IEnumerator Shot4()
    {
        if (isRunning)
            yield break;
        isRunning = true;

        int t = 0;
        int t2 = 0;
        float dir = 0;
        int tm = 0;
        float pi2 = Mathf.PI * 2;


        while (ShotEndTime >= 0)
        {
            t = t % 200;
            if (t == 0)
                tm = (int)(190 + Utility.Random(30));

            // 上からふらす弾を発射する基準の角度をセット
            dir = Mathf.PI * 1.5f + Mathf.PI / 6 * Mathf.Sin(pi2 / tm * t2);

            // ギラギラ光る弾
            if (t2 % 4 == 0)
            {
                // 8方向にふる
                for (int n = 0; n < 8; ++n)
                {
                    float vx = Mathf.Cos(dir + Mathf.PI / 8 * 4 + Mathf.PI / 8 * n + Mathf.PI / 16) * 2.5f;
                    float vy = Mathf.Sin(dir + Mathf.PI / 8 * 4 + Mathf.PI / 8 * n + Mathf.PI / 16) * 2.5f;

                    EnemyBullet eb = EnemyBullet.Add(22, X, Y, 90, 0, 0, 0);
                    eb.SetVelocityXY(vx, vy);
                    eb.IsAddBlend = true;
                }
            }

            // 小さい玉
            if (t % 1 == 0 && t2 > 80)
            {
                int num = 1;
                if (t % 2 == 0)
                    num = 2;

                for (int n = 0; n < num; ++n)
                {
                    dir = Mathf.PI * 1.5f - Mathf.PI / 2 + Mathf.PI / 12 * (t2 % 13) + Utility.Random(Mathf.PI / 15);
                    EnemyBullet eb = EnemyBullet.Add(14, X, Y, 270, 0, 0, 1);

                    float vx = Mathf.Cos(dir) * 1.4f * 1.2f;
                    float vy = Mathf.Sin(dir) * 1.4f;
                    eb.SetVelocityXY(vx, vy);
                    eb.Angle = eb.Direction + 90;
                }
            }

            var it = EnemyBullet.Manager.GetExistObjects;
            foreach (var e in it)
            {
                if (e.state == 0)
                {
                    if (e.cnt < 150)
                    {
                        e.VY -= 0.025f;
                    }
                }
                if (e.state == 1)
                {
                    if (e.cnt < 160)
                        e.VY -= 0.03f;
                }
            }

            t++;
            t2++;
            yield return new WaitForEndOfFrame();
        }
        isRunning = false;
    }
    // ☆弾
    private IEnumerator Shot5()
    {
        if (isRunning)
            yield break;
        isRunning = true;

        const int TM005 = 820;
        const float RANGE005 = 2.5f;
        const float LEM005 = 0.35f;

        int t = 0;
        float sDir = 0;
        float sx = 0;
        float sy = 0;
        int sst = 0;
        int bnum = 0;
        float pi2 = Mathf.PI * 2;

        while (ShotEndTime >= 0)
        {
            t = t % TM005;
            // 1周期の最初なら
            if (t == 0)
            {
                sst = 0;
                sx = X;
                sy = Y - RANGE005;
                sDir = Mathf.PI / 5 / 2 + Mathf.PI / 2;
                bnum = 0;
            }

            // 星を描く
            if (sst <= 4)
            {
                for (int i = 0; i < 2; ++i)
                {
                    // 座標を計算
                    sx += Mathf.Cos(sDir) * LEM005;
                    sy += Mathf.Sin(sDir) * LEM005;

                    // 円と交わったら
                    if ((sx - X) * (sx - X) + (sy - Y) * (sy - Y) > RANGE005 * RANGE005)
                    {
                        // 方向転換
                        sDir -= (Mathf.PI - Mathf.PI / 5);
                        // ステータス変換
                        sst++;
                        // 5なら終わる
                        if (sst == 5)
                            break;
                    }
                    // 星を5つ描く
                    for (int j = 0; j < 5; ++j)
                    {
                        float dir = -Mathf.PI / 2 + pi2 / 5 * j;

                        EnemyBullet eb = EnemyBullet.Add(25 + j, sx, sy, dir * Mathf.Rad2Deg, 0.00f, 0, j);
                        eb.CollisionRadius = 0.06f;
                        eb.baseDir[0] = sDir - Mathf.PI + Mathf.PI / 20 * bnum;
                        eb.tmpDirection = -Mathf.PI / 2 + pi2 / 5 * j;
                    }
                    bnum++;
                }
            }

            var it = EnemyBullet.Manager.GetExistObjects;
            foreach (var obj in it)
            {
                int cnt = obj.cnt;
                // ステータスが10未満なら
                if (obj.state < 10)
                {
                    // 150なら星を５方向に発射
                    if (t == 150)
                    {
                        //eb.SetVelocity(eb.Direction, 4);
                        obj.SetVelocity(obj.tmpDirection * Mathf.Rad2Deg, 5);
                        obj.cnt = 0;
                        obj.state += 10;
                    }
                }
                // 10~19なら
                else if (obj.state < 20)
                {
                    if (cnt <= 80)
                    {
                        // 減速
                        float sp = obj.Speed;
                        sp -= 0.05f;
                        obj.SetVelocity(obj.tmpDirection * Mathf.Rad2Deg, sp);
                    }
                    if (cnt == 100)
                    {
                        // セットした基準角度から綺麗な曲線を描く
                        obj.SetVelocity(obj.baseDir[0] * Mathf.Rad2Deg, 0);
                    }
                    if (cnt >= 100 && cnt < 160)
                    {
                        // 加速
                        float sp = obj.Speed;
                        sp += 0.015f;
                        obj.SetVelocity(obj.baseDir[0] * Mathf.Rad2Deg, sp);
                    }
                }
            }
            t++;
            yield return new WaitForEndOfFrame();
        }
        isRunning = false;
    }
    #endregion

    // ダメージを与える
    private bool Damage(int v)
    {
        CurrentHP -= v;
        if (CurrentHP <= 0)
        {
            // 敵の削除
            //DestroyObj();
            Vanish();
            for (int i = 0; i < 6; ++i)
                Particle.Add(X, Y);

            return true;
        }
        return false;
    }

    // 衝突判定
    private void OnTriggerEnter2D(Collider2D collider)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(collider.gameObject.layer);

        if (layerName != "Bullet(Player)")
            return;

        // 弾の削除
        Bullet b = collider.GetComponent<Bullet>();
        b.Vanish();

        // ダメージを与える
        CurrentHP -= 1;
    }

    // アクティブにする
    public override void Revive()
    {
        ActiveTime = 0;
        base.Revive();
    }
}

