﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        // サウンドロード
        Sound.LoadBgm("BGM", "BGM/Stage");
        Sound.LoadSe("PlayerShot", "SE/DM-CGS-40");
        Sound.LoadSe("PlayerHit", "SE/PlayerHit");
        Sound.LoadSe("EnemyShot", "SE/DM-CGS-21");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
