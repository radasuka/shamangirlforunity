﻿using UnityEngine;
using System.Collections;

public class Particle : Actor
{
    /// <summary>
    /// 管理オブジェクト
    /// </summary>
    public static ActorManager<Particle> Manager = null;

    /// <summary>
    /// インスタンスの取得
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public static Particle Add(float x, float y)
    {
        Particle p = Manager.Add(x, y);

        if(p)
        {
            // ランダムに移動する
            p.SetVelocity(Random.Range(0, 359), Random.Range(2.0f, 4.0f));
            // 初期サイズの設定
            p.SetScale(1f, 1f);
        }
        return p;
    }

    /// <summary>
    /// 更新処理
    /// </summary>
    void Update()
    {
        MulVelocity(0.95f);
        MulScale(0.97f);
        if (Scale < 0.01f)
            Vanish();
    }
}
