﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyBullet : Actor
{
    /// <summary>
    /// 管理オブジェクト
    /// </summary>
    public static ActorManager<EnemyBullet> Manager = null;

    // スプライト
    public Sprite[] sprites = new Sprite[40];

    // 生成されてからの経過時間
    public float ActiveTime;
    // ゲームオブジェクト生成から削除するまでの時間
    public float LifeTime = 10f;

    // ステータス
    public int state;
    // 弾を光らせるかどうか
    public bool IsAddBlend = false;
    // カウンタ
    public int cnt;
    // ベースの角度
    public float[] baseDir = new float[2];
    // 一時記憶向き
    public float tmpDirection;

    /// <summary>
    /// インスタンスの取得
    /// </summary>
    /// <param name="id"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="direction"></param>
    /// <param name="speed"></param>
    /// <returns></returns>
    public static EnemyBullet Add(int id, float x, float y, float direction, float speed, int bulletColor, int state)
    {
        var eBullet = Manager.Add(x, y, direction, speed);

        eBullet.SetSprite(eBullet.sprites[id]);
        eBullet.ActiveTime = 0;
        eBullet.state = state;
        eBullet.cnt = 0;

        // コリジョンを設定する
        eBullet.BoxColliderEnabled = false;
        eBullet.CircleColliderEnabled = true;
        eBullet.CollisionRadius = 0.09f;

        return eBullet;
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        ActiveTime += Time.deltaTime;
        cnt++;

        // 光らせる
        if(IsAddBlend)
        {
            float col = Random.Range(0, 0.2f);
            Renderer.color = new Color(0.5f + col, 0.5f + col, 0.5f + col);
        }
        // 画像の向きを変える
        Angle = Direction + 90;

        // 消滅時間を超えたら消滅
        if (ActiveTime >= LifeTime && IsOutside())
            Vanish();
    }
}
