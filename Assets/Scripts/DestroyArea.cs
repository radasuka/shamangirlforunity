﻿using UnityEngine;

public class DestroyArea : MonoBehaviour
{
    void OnTriggerExit2D(Collider2D collider)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(collider.gameObject.layer);

        if (layerName == "Bullet(Enemy)")
        {
            // 弾の削除
            //EnemyBullet eb = collider.GetComponent<EnemyBullet>();
            //if (eb.ActiveTime >= eb.LifeTime)
            //    eb.Vanish();
        }
        // レイヤー名がEnemyの場合
        if (layerName == "Enemy")
        {
            Enemy e = collider.GetComponent<Enemy>();
            e.Vanish();
        }
    }
}
