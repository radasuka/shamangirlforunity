﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    //フェード中の透明度
    private float fadeAlpha = 0;
    // Image
    private Image game;
    private Image over;

    private Color color;

    // Use this for initialization
    void Start()
    {
        // Imageを取得
        foreach (Transform child in transform)
        {
            if (child.name == "Game")
                game = child.gameObject.GetComponent<Image>();
            if(child.name == "Over")
                over = child.gameObject.GetComponent<Image>();
        }
        // 初期カラーを設定
        color = new Color(1, 1, 1, 0);
        game.color = color;
        over.color = color;

        //  フェードイン開始
        StartCoroutine(Fade());
    }

    // フェードイン
    private IEnumerator Fade()
    {
        float time = 0;
        while (time <= 1)
        {
            this.fadeAlpha = Mathf.Lerp(0f, 1f, time / 1);
            time += Time.deltaTime;
            yield return 0;
        }
    }

    private void OnGUI()
    {
        color.a = fadeAlpha;
        game.color = color;
        over.color = color;
    }
}
