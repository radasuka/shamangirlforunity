﻿using UnityEngine;
using UnityEngine.UI;

class StageStartState : IState<GameManager>
{
    private static StageStartState _instance = null;
    /// <summary>
    /// 単一インスタンスを取得します。
    /// </summary>
    public static StageStartState Instance => _instance ?? (_instance = new StageStartState());

    private StageStartState() { }

    public void Update(float elapsedTime, GameManager parent)
    {
        Text text = parent.StageNumObj.GetComponent<Text>();

        text.text = "すて～じ " + parent.CurrentStageNum;

        parent.StageNumObj.SetActive(true);

        parent.Player.transform.position = Vector2.MoveTowards(parent.Player.transform.position, new Vector2(0, -4), 2 * Time.deltaTime);
        if (parent.StateTime >= 4)
        {
            //if (c != null)
            //    StopCoroutine(c);
            parent.Player.Mode = Player.ModeType.ModeType_Default;
            parent.StageNumObj.SetActive(false);
            //parent.State = GameManager.eState.Play;
            parent.SetPlayingState();
        }
    }
}
