﻿using System.Collections.Generic;
using UnityEngine;

class PlayingState : IState<GameManager>
{
    private static PlayingState _instance = null;
    /// <summary>
    /// 単一インスタンスを取得します。
    /// </summary>
    public static PlayingState Instance => _instance ?? (_instance = new PlayingState());

    private PlayingState() { }

    public void Update(float elapsedTime, GameManager parent)
    {
        var reservedEnemy = Enemy.Manager.ReservedList.First;
        LinkedListNode<Enemy> activeEnemy = null;
        while (reservedEnemy != null)
        {
            if (reservedEnemy.Value.SpawnTime <= parent.EnemySpawnTime)
            {
                activeEnemy = reservedEnemy;
                reservedEnemy = reservedEnemy.Next;
                Enemy.Manager.AddActiveItem(activeEnemy.Value);
                continue;
            }
            reservedEnemy = reservedEnemy.Next;
        }
        // ボス出現時間になったらボス出現
        if ((parent.Boss.SpawnTime[0] <= parent.EnemySpawnTime && parent.Boss.IsMedium || parent.Boss.SpawnTime[1] <= parent.EnemySpawnTime) && 
             parent.Boss.IsExists == false)
        {
            parent.Boss.InitBoss();
            parent.Boss.Revive();
            parent.HpBarObj.SetActive(true);
            Enemy.Manager.Vanish();
        }

        if (parent.Player.IsExists == false)
        {
            // 残機が無くなったらゲームオーバー
            if (parent.Player.LifeCount >= 0)
            {
                parent.Player.Revive();
                parent.Player.SetPosition(0, -8);
            }
            else
                parent.SetGameOverState();
        }
        // クリア判定
        if(parent.Boss.isDead)
            parent.SetStageClearState();
    }
}
