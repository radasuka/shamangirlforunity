﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;

/// <summary>
/// ゲームを管理するクラス
/// </summary>
public class GameManager : MonoBehaviour
{
    // 最大ステージ数
    public const int MAXSATGENUM = 2;

    // ゲームの状態
    private IState<GameManager> gameState = null;
    public IState<GameManager> GameState
    {
        get { return gameState; }
        private set
        {
            if (gameState == value)
                return;
            gameState = value;
            StateTime = 0;
        }
    }

    // PlayGameSceneがアクティブになってからの経過時間
    private float elapsedTime;
    // ステートがかわってからの経過時間
    public float StateTime { get; private set; }
    // 敵の出現を管理する時間
    public float EnemySpawnTime { get; private set; }

    // プレイヤー
    public Player Player { get; private set; }
    // ボス
    public Boss Boss { get; private set; }

    //public EnemyMgr Enemies;

    // UI
    public Canvas mainUI;
    public Texture2D lifeIcons;
    public GameObject GameOver { get; private set; }
    public GameObject StageNumObj { get; private set; }
    public GameObject StageClearObj { get; private set; }
    public GameObject HpBarObj { get; private set; }
    public Slider HpBar { get; private set; }

    // 現在のステージ数
    public int CurrentStageNum { get; private set; }

    // Use this for initialization
    void Start()
    {
        // 各インスタンス生成
        Player = Player.Add(0, -8, 90, 0);
        Bullet.Manager = new ActorManager<Bullet>("Bullets/Bullet", 50);
        EnemyBullet.Manager = new ActorManager<EnemyBullet>("Bullets/EnemyBullet", 3072);
        Particle.Manager = new ActorManager<Particle>("Particle", 256);

        Enemy.Manager = new EnemyManager();

        // サウンドロード
        Sound.LoadBgm("BGM", "BGM/Stage");
        Sound.LoadSe("PlayerShot", "SE/DM-CGS-40");
        Sound.LoadSe("PlayerHit", "SE/PlayerHit");
        Sound.LoadSe("EnemyShot", "SE/DM-CGS-21");

        // UI取得
        foreach (Transform child in mainUI.transform)
        {
            if (child.name == "GameOver")
                GameOver = child.gameObject;
            if (child.name == "StageNum")
                StageNumObj = child.gameObject;
            if (child.name == "StageClear")
                StageClearObj = child.gameObject;
            if (child.name == "HpBar")
            {
                HpBarObj = child.gameObject;
                HpBar = HpBarObj.GetComponent<Slider>();
            }
        }

        CurrentStageNum = 1;
        SceneReset();
    }

    // ゲーム画面を初期化
    private void SceneReset()
    {
        elapsedTime = 0;
        EnemySpawnTime = 0;
        SetStageStartState();

        Sound.PlayBgm("BGM");

        var fileName = "EnemyDataStage" + CurrentStageNum;
        Enemy.Manager.LoadEnemy("Enemy", fileName);

        Boss = Boss.Add(0, 6, 270, 0, CurrentStageNum, HpBar);
        Boss.Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }


    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        StateTime += Time.deltaTime;

        if (Boss.IsExists == false)
            EnemySpawnTime += Time.deltaTime;

        GameState.Update(elapsedTime, this);
    }

    // 次のステージへすすめる
    public void NextStage()
    {
        ++CurrentStageNum;
        SceneReset();
    }

    // GUI 描画
    private void OnGUI()
    {
        for (int i = 0; i < Player.LifeCount; ++i)
            GUI.Label(new Rect(945 + 25 * i, 55, 25, 25), lifeIcons);

        //------------DebugText------------------
        Utility.SetFontColor(Color.black);
        Utility.SetFontSize(20);
        Utility.GUILabel(0, 0, 50, 10, "elapsedTime:" + elapsedTime.ToString());
        Utility.GUILabel(0, 20, 50, 10, "StateTime:" + StateTime.ToString());
        Utility.GUILabel(0, 40, 50, 10, "EnemySpawnTime:" + EnemySpawnTime.ToString());
        Utility.GUILabel(0, 60, 50, 10, "StageNo:" + CurrentStageNum.ToString());
        Utility.GUILabel(0, 80, 50, 10, "GameState:" + GameState.ToString());
        //---------------------------------------
    }

    /// <summary>
    /// GameState を StageStar に設定
    /// </summary>
    public void SetStageStartState()
    {

        GameState = StageStartState.Instance;
        //StateTime = 0;
    }
    /// <summary>
    /// GameState を PlayingState に設定
    /// </summary>
    public void SetPlayingState()
    {

        GameState = PlayingState.Instance;
        //StateTime = 0;
    }
    /// <summary>
    /// GameState を GameOverState に設定
    /// </summary>
    public void SetGameOverState()
    {
        GameState = GameOverState.Instance;
        //StateTime = 0;
    }
    /// <summary>
    /// GameState を StageClearState に設定
    /// </summary>
    public void SetStageClearState()
    {
        GameState = StageClearState.Instance;
        //StateTime = 0;
    }
}
