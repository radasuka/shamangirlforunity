﻿using UnityEngine;
using UnityEngine.UI;

class StageClearState : IState<GameManager>
{
    private static StageClearState _instance = null;
    /// <summary>
    /// 単一インスタンスを取得します。
    /// </summary>
    public static StageClearState Instance => _instance ?? (_instance = new StageClearState());

    private StageClearState() { }

    public void Update(float elapsedTime, GameManager parent)
    {
        parent.StageClearObj.GetComponent<FadeText>().isInOut = false;
        parent.StageClearObj.SetActive(true);

        if (parent.CurrentStageNum >= GameManager.MAXSATGENUM)
        {
            Score.Instance.Save();
            FadeManager.Instance.LoadLevel("GameClearScene", 1f, Color.black, new Rect(0, 0, Screen.width, Screen.height));
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                FadeManager.Instance.NextStage(1f, Color.black, new Rect(370, 0, 538, Screen.height));
                parent.StageClearObj.SetActive(false);
                parent.NextStage();
            }
        }
    }

}

