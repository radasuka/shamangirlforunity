﻿using UnityEngine;

class GameOverState : IState<GameManager>
{
    private static GameOverState _instance = null;
    /// <summary>
    /// 単一インスタンスを取得します。
    /// </summary>
    public static GameOverState Instance => _instance ?? (_instance = new GameOverState());

    private GameOverState() { }

    public void Update(float elapsedTime, GameManager parent)
    {
        parent.GameOver.SetActive(true);
        if (Enemy.Manager.ActiveList.Count > 0)
            Enemy.Manager.Vanish();

        if (Input.GetKeyDown(KeyCode.Return))
        {
            parent.GameOver.SetActive(false);
            FadeManager.Instance.LoadLevel("TitleScene", 1f, Color.black, new Rect(0, 0, Screen.width, Screen.height));
        }
    }
}

