﻿using UnityEngine;
using System.Collections;

public class Bullet : Actor
{
    private float activeTime;

    /// <summary>
    /// 管理オブジェクト
    /// </summary>
    public static ActorManager<Bullet> Manager = null;

    // ゲームオブジェクト生成から削除するまでの時間
    public float LifeTime = 3f;

    /// <summary>
    /// インスタンスの取得
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="direction"></param>
    /// <param name="speed"></param>
    /// <returns></returns>
    public static Bullet Add(float x, float y, float direction, float speed)
    {
        // インスタンスの取得
        Bullet bullet = Manager.Add(x, y, direction, speed);

        // TODO:設定等
        bullet.SetSize(bullet.SpriteWidth, bullet.SpriteHeight);

        return bullet;
    }


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        activeTime += Time.deltaTime;

        if (activeTime >= LifeTime || IsOutside())
        {
            activeTime = 0;
            Vanish();
        }
    }

    // 消滅
    public override void Vanish()
    {
        // パーティクル生成
        Particle p = Particle.Add(X, Y);
        if(p != null)
        {
            // 青色にする
            p.SetColor(0.1f, 0.1f, 1f);
            // 速度を少し遅くする
            p.MulVelocity(0.7f);
        }
        base.Vanish();
    }
}
