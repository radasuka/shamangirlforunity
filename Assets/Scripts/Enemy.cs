﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct EnemyData
{
    // 敵登場時間
    public float SpawnTime;
    // 移動パターン
    public int Pattern;
    // 敵の種類
    public int TypeID;
    // 初期座標
    public Vector2 Position;
    // 移動スピード
    public float Speed;
    // 弾幕発射時間
    public float ShotTime;
    // 弾幕の種類
    public int ShotType;
    // 弾の色
    public int BulletColor;
    // 体力
    public int HP;
    // 弾の種類
    public int BulletType;
    // 待機時間
    public float Wait;
    // ポイント
    public int Point;
}


public class Enemy : Actor
{
    /// <summary>
    /// 管理オブジェクト
    /// </summary>
    public static EnemyManager Manager = null;

    // アニメーター
    private Animator animator;

    // 追尾弾のターゲット
    public static Player target = null;

    public enum Type
    {
        Blue,
        Yellow,
        Green,
        Big,
        Storongu,
        Boss
    }

    // スプライト
    public Sprite[] Sprites = new Sprite[5];

    // 出現時間
    public float SpawnTime { get; set; }
    // 移動パターン
    public int Pattern;
    // 敵の種類
    public Type TypeId;
    // 弾幕発射時間
    public float ShotTime;
    // 弾幕の種類
    public int ShotType;
    // 弾の色
    public int BulletColor;
    // 体力
    public int HP { get; set; }
    // 弾の種類
    public int BulletType;
    // 待機時間
    public float Wait;
    // アクティブになってからの経過時間
    public float activeTime;
    // ポイント
    public int Point;

    private delegate IEnumerator EnemyFunc();
    // 移動パターン
    private List<EnemyFunc> enemyPatterns = new List<EnemyFunc>();
    // 弾幕パターン
    private List<EnemyFunc> shots = new List<EnemyFunc>();

    /// <summary>
    /// 各パラメータを設定
    /// </summary>
    /// <param name="speed">スピード</param>
    /// <param name="pos">位置</param>
    /// <param name="direction">向き</param>
    public void SetParameter(float speed, Vector2 pos, float direction = 270)
    {
        // 座標設定
        SetPosition(pos.x, pos.y);
        // 速度設定
        SetVelocity(direction, speed);

        // 移動パターン
        enemyPatterns.Add(EnemyPattern0);
        enemyPatterns.Add(EnemyPattern1);
        enemyPatterns.Add(EnemyPattern2);
        enemyPatterns.Add(EnemyPattern3);
        enemyPatterns.Add(EnemyPattern4);
        enemyPatterns.Add(EnemyPattern5);
        enemyPatterns.Add(EnemyPattern6);
        enemyPatterns.Add(EnemyPattern7);
        enemyPatterns.Add(EnemyPattern8);
        enemyPatterns.Add(EnemyPattern9);
        enemyPatterns.Add(EnemyPattern10);
        enemyPatterns.Add(EnemyPattern11);
        // 弾幕パターン
        shots.Add(Shot0);
        shots.Add(Shot1);
        shots.Add(Shot2);
        shots.Add(Shot3);
        shots.Add(Shot4);
        shots.Add(Shot5);
        shots.Add(Shot6);
        shots.Add(Shot7);
        shots.Add(Shot8);
        shots.Add(Shot9);
        shots.Add(Shot10);
        shots.Add(Shot11);

        // スプライト設定
        SetSprite(Sprites[(int)TypeId]);

        if (TypeId <= Type.Green)
        {
            string path = "Animations/Enemy/";
            if (TypeId == Type.Blue)
                path = string.Concat(path, "Blue");
            else if (TypeId == Type.Yellow)
                path = string.Concat(path, "Yellow");
            else
                path = string.Concat(path, "Green");

            animator = GetComponent<Animator>();
            animator.runtimeAnimatorController = Instantiate(Resources.Load(path)) as RuntimeAnimatorController;
        }
    }

    void Start()
    {
        StartCoroutine(enemyPatterns[Pattern]());
        StartCoroutine(shots[ShotType]());

        //StartCoroutine(EnemyPattern11());
        //StartCoroutine(shots[0]());
    }

    void Update()
    {
        activeTime += Time.deltaTime;
        //StartCoroutine(EnemyPattern0());
        //StartCoroutine(shots[10]());
    }

    private IEnumerator Move()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            float dir = GetAim();
            SetVelocity(dir, 1);
            yield return new WaitForSeconds(2);
            SetVelocityXY(0, 0);
            StartCoroutine(Shot3());
            SetVelocity(90, 1);
        }
    }

    private IEnumerator Move2(Vector3 target)
    {
        float dx = target.x - X;
        float dy = target.y - Y;
        float dir = Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;
        SetVelocity(dir, 1);
        yield return new WaitForSeconds(3);
        SetVelocity(0, 0);
        Coroutine c = StartCoroutine(Shot2());
        yield return new WaitForSeconds(5);
        StopCoroutine(c);
        SetVelocity(90, 1);
    }

    #region 移動パターン
    //移動パターン0
    //下がってきて停滞して上がっていく
    private IEnumerator EnemyPattern0()
    {
        // 下がってくる
        VY = -2;
        // 1秒中断
        yield return new WaitForSeconds(1);
        //止まる
        VY = 0;
        // Wait秒中断
        yield return new WaitForSeconds(Wait);
        // 上がっていく
        VY = 2;
    }
    //移動パターン1
    //下がってきて停滞して左下に行く
    private IEnumerator EnemyPattern1()
    {
        // 下がってくる
        VY = -2;
        // 1秒中断
        yield return new WaitForSeconds(1);
        //止まる
        VY = 0;
        // Wait秒中断
        yield return new WaitForSeconds(Wait);
        VX = -2;
        VY = -2;
    }
    //移動パターン2
    //下がってきて停滞して右下に行く
    private IEnumerator EnemyPattern2()
    {
        // 下がってくる
        VY = -2;
        // 1秒中断
        yield return new WaitForSeconds(1);
        //止まる
        VY = 0;
        // Wait秒中断
        yield return new WaitForSeconds(Wait);
        VX = 2;
        VY = -2;
    }
    //行動パターン3
    //すばやく降りてきて左へ
    private IEnumerator EnemyPattern3()
    {
        // 下がってくる
        VY = -3.5f;
        yield return new WaitForSeconds(0.9f);

        while (true)
        {
            VX -= 0.8f;
            VY *= 0.5f;

            yield return new WaitForSeconds(0.3f);
        }
    }
    //行動パターン4
    //すばやく降りてきて右へ
    private IEnumerator EnemyPattern4()
    {
        // 下がってくる
        VY = -3.5f;
        yield return new WaitForSeconds(0.9f);

        while (true)
        {
            VX += 0.8f;
            VY *= 0.5f;

            yield return new WaitForSeconds(0.3f);
        }
    }
    //行動パターン5
    //斜め左下へ
    private IEnumerator EnemyPattern5()
    {
        SetVelocityXY(-1, -2);

        yield return null;
    }
    //行動パターン6
    //斜め右下へ
    private IEnumerator EnemyPattern6()
    {
        SetVelocityXY(1, -2);

        yield return null;
    }
    //移動パターン7
    //停滞してそのまま左上に
    private IEnumerator EnemyPattern7()
    {
        VY = -2;
        yield return new WaitForSeconds(1);
        VY = 0;
        yield return new WaitForSeconds(Wait);

        SetVelocityXY(-0.7f, 0.5f);

        yield return null;
    }
    //移動パターン8
    //停滞してそのまま右上に
    private IEnumerator EnemyPattern8()
    {
        VY = -2;
        yield return new WaitForSeconds(1);
        VY = 0;
        yield return new WaitForSeconds(Wait);

        SetVelocityXY(0.7f, 0.5f);

        yield return null;
    }
    //移動パターン9
    //停滞してそのまま上に
    private IEnumerator EnemyPattern9()
    {
        VY = 0;
        yield return new WaitForSeconds(Wait);
        VY = 1;
    }
    //移動パターン10
    //下がってきてウロウロして上がっていく
    private IEnumerator EnemyPattern10()
    {
        VY = -2;
        yield return new WaitForSeconds(1);
        VY = 0;
        yield return new WaitForSeconds(2);

        while (true)
        {
            yield return new WaitForSeconds(1f);
            float dir = Mathf.Cos(Direction * Mathf.Deg2Rad) < 0 ? 0 : 1;
            SetVelocity((Random.Range(-(Mathf.PI / 4), (Mathf.PI / 4)) + Mathf.PI * dir) * Mathf.Rad2Deg, 1.5f + Random.Range(-1.0f, 1.0f));
            //SetVelocity(Random.Range(0, 365), 2);

            yield return new WaitForSeconds(0.5f);
            MulVelocity(0.85f);

            if (activeTime >= 7)
                break;
        }
        yield return new WaitForSeconds(Wait);
        //VY += 1;
        SetVelocityXY(0, 2);
    }
    //移動パターン11
    //みょん弾幕
    private IEnumerator EnemyPattern11()
    {
        // 下がってくる
        VY = -2;
        // 1秒中断
        yield return new WaitForSeconds(1);
        //止まる
        VY = 0;
        // Wait秒中断
        yield return new WaitForSeconds(5);
        // 上がっていく
        //VY = 2;
    }
    #endregion

    // 狙い撃つ角度を取得
    public float GetAim()
    {
        float dx = target.X - X;
        float dy = target.Y - Y;

        return Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;
    }

    // 狙い撃ち
    private IEnumerator Shot0()
    {
        while (true)
        {
            // 2秒待つ
            yield return new WaitForSeconds(2f);
            float dir = GetAim();
            EnemyBullet.Add(0, X, Y, dir, 5, 0, 0);
            Sound.PlaySe("EnemyShot");
        }
    }
    // 回転弾
    private IEnumerator Shot1()
    {
        //yield return new WaitForSeconds(2f);
        float dir = 0;
        while (true)
        {
            EnemyBullet.Add(1, X, Y, dir, 2, 0, 0);
            Sound.PlaySe("EnemyShot");
            dir += 16;
            yield return new WaitForSeconds(0.1f);
        }
    }
    // 3way弾
    private IEnumerator Shot2()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            EnemyBullet.Add(2, X, Y, 270 - 5, 5, 0, 0);
            EnemyBullet.Add(2, X, Y, 270, 5, 0, 0);
            EnemyBullet.Add(2, X, Y, 270 + 5, 5, 0, 0);
            Sound.PlaySe("EnemyShot");
        }
    }
    // 円形弾
    private IEnumerator Shot3()
    {
        yield return new WaitForSeconds(1);
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            float dir = GetAim();
            int n = 20;
            for (int i = 0; i < n; ++i)
            {
                dir += (Mathf.PI * 2 / n) * Mathf.Rad2Deg;
                EnemyBullet.Add(0, X, Y, dir, 5, 0, 0);
            }
            Sound.PlaySe("EnemyShot");
        }
    }
    // ばらまきショット
    private IEnumerator Shot4()
    {
        float time = 0;
        yield return new WaitForSeconds(2);
        while (time < 0.2f)
        {
            yield return new WaitForSeconds(0.1f);
            float dir = GetAim();
            float pi4 = Mathf.PI / 4;
            dir += (Random.Range(-pi4, pi4)) * Mathf.Rad2Deg;
            EnemyBullet.Add(0, X, Y, dir, 3 + Random.Range(-1.5f, 1.5f), 0, 0);
            Sound.PlaySe("EnemyShot");
            time += Time.deltaTime;
        }
    }
    // ばらまきショット(減速)
    private IEnumerator Shot5()
    {
        float time = 0;
        yield return new WaitForSeconds(2);
        while (time < 0.2f)
        {
            yield return new WaitForSeconds(0.1f);

            float dir = GetAim();
            float pi4 = Mathf.PI / 4;
            dir += (Random.Range(-pi4, pi4)) * Mathf.Rad2Deg;
            EnemyBullet.Add(0, X, Y, dir, 4 + Random.Range(-2.0f, 2.0f), 0, 0);

            Sound.PlaySe("EnemyShot");
            time += Time.deltaTime;
        }
        EnemyBullet.Manager.ForEachExist(e => { if (e.Speed > 1.5f) e.MulVelocity(0.5f); });
    }
    // 自機に向かって直線発射(常に自機狙い)
    private IEnumerator Shot6()
    {
        yield return new WaitForSeconds(2f);
        for (int i = 0; i <= 10; ++i)
        {
            yield return new WaitForSeconds(0.1f);
            float dir = GetAim();
            EnemyBullet.Add(0, X, Y, dir, 5, 0, 0);
            Sound.PlaySe("EnemyShot");
        }
    }
    // 自機に向かって直線発射(角度記憶)
    private IEnumerator Shot7()
    {
        yield return new WaitForSeconds(2f);
        float dir = GetAim();
        for (int i = 0; i <= 10; ++i)
        {
            yield return new WaitForSeconds(0.1f);
            EnemyBullet.Add(0, X, Y, dir, 5, 0, 0);
            Sound.PlaySe("EnemyShot");
        }
    }
    // 自機に向かってスピード変化直線発射
    private IEnumerator Shot8()
    {
        yield return new WaitForSeconds(1.5f);
        float time = 0;
        for (int i = 0; i <= 10; ++i)
        {
            yield return new WaitForSeconds(0.16f);
            float dir = GetAim();
            float speed = 2 + 20.0f * time;
            EnemyBullet.Add(0, X, Y, dir, speed, 0, 0);
            Sound.PlaySe("EnemyShot");
            time += Time.deltaTime;
        }
    }
    //みょん弾幕
    private IEnumerator Shot9()
    {
        yield return new WaitForSeconds(2.1f);
        float time = 0;
        float x = 0;
        float y = 0;
        float dir = 0;
        while (time <= 0.2f)
        {
            yield return new WaitForSeconds(0.25f);
            float pi3 = 270 * Mathf.Deg2Rad;
            float pi2 = 90 * Mathf.Deg2Rad;
            for (int i = 0; i < 20; ++i)
            {
                dir = (Mathf.PI * 2 / 20 * i) * Mathf.Rad2Deg;
                x = X + Mathf.Cos(pi3 + pi2 / 0.1f * time) * 3;
                y = Y + Mathf.Sin(pi3 + pi2 / 0.15f * time) * 3;
                EnemyBullet.Add(5, x, y, dir, 1.0f, 0, 0);
                Sound.PlaySe("EnemyShot");
            }
            for (int j = 0; j < 20; ++j)
            {
                dir = ((Mathf.PI * 2) / 20 * j) * Mathf.Rad2Deg;
                x = X + Mathf.Cos(pi3 - pi2 / 0.1f * time) * 3;
                y = Y + Mathf.Sin(pi3 - pi2 / 0.15f * time) * 3;
                EnemyBullet.Add(6, x, y, dir, 1.0f, 0, 0);
                Sound.PlaySe("EnemyShot");
            }
            time += Time.deltaTime;
        }
        yield break;
    }
    //ミシャグジさま
    // TODO : 終了タイミング
    private IEnumerator Shot10()
    {
        yield return new WaitForSeconds(2.1f);

        float time = 0;

        while (time < 0.5f)
        {
            yield return new WaitForSeconds(1.25f);

            float dir = Random.Range(-Mathf.PI, Mathf.PI);
            for (int j = 0; j < 2; ++j)
            {
                for (int i = 0; i < 70; ++i)
                {
                    // 円形70個
                    dir += ((Mathf.PI * 2) / 70) * Mathf.Rad2Deg;
                    EnemyBullet eb = EnemyBullet.Add(14, X, Y, dir, 1.5f, 0, j);
                    eb.Angle = dir + 90.0f;

                    // Colliderを短形に変更する
                    eb.CircleColliderEnabled = false;
                    eb.BoxColliderEnabled = true;
                    eb.BoxColliderWidth = 0.07f;
                    eb.BoxColliderHeight = 0.15f;
                }
                Sound.PlaySe("EnemyShot");
            }

            var it = EnemyBullet.Manager.GetExistObjects;
            foreach (var eb in it)
            {
                if (0.5f < eb.ActiveTime && eb.ActiveTime < 3.5f)
                {
                    int state = eb.state;
                    float speed = eb.Speed;
                    float dir2 = eb.Direction;

                    speed -= 0.7f / 3f;
                    eb.Angle += (-(Mathf.PI / 2) / eb.ActiveTime * (state == 0 ? -1 : 1)) * Mathf.Rad2Deg;
                    dir2 += (-(Mathf.PI / 2) / eb.ActiveTime * (state == 0 ? -1 : 1)) * Mathf.Rad2Deg;
                    eb.SetVelocity(dir2, speed);
                }
            }
            time += Time.deltaTime;
        }
    }
    // 弾を撃たない
    private IEnumerator Shot11()
    {
        yield break;
    }

    // ダメージを与える
    private bool Damage(int v)
    {
        HP -= v;
        if (HP <= 0)
        {
            Score.Instance.AddScore(Point);
            // 敵の削除
            Manager.RemoveActiveItem(this);
            for (int i = 0; i < 6; ++i)
            {
                Particle p = Particle.Add(X, Y);
                p.SetColor(1, 1, 1);
            }
            return true;
        }
        return false;
    }

    // 衝突判定
    private void OnTriggerEnter2D(Collider2D collider)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(collider.gameObject.layer);

        if (layerName != "Bullet(Player)")
            return;

        // 弾の削除
        var bullet = collider.GetComponent<Bullet>();
        bullet.Vanish();

        // ダメージを与える
        Damage(1);
    }

}
