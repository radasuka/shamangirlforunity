﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EnemyManager
{
    private GameObject prefab = null;

    /// <summary>
    /// 予約リスト
    /// </summary>
    public LinkedList<Enemy> ReservedList;
    /// <summary>
    /// アクティブリスト
    /// </summary>
    public LinkedList<Enemy> ActiveList;
    /// <summary>
    /// リムーブリスト
    /// </summary>
    public LinkedList<Enemy> RemovedList;

    public delegate void FuncT(Enemy e);

    public EnemyManager()
    {
        ReservedList = new LinkedList<Enemy>();
        ActiveList = new LinkedList<Enemy>();
        RemovedList = new LinkedList<Enemy>();
    }

    public void LoadEnemy(string prefabName,  string fileName)
    {
        prefab = Resources.Load("Prefabs/Enemy/" + prefabName) as GameObject;
        if (prefab == null)
            Debug.LogError("EnemyPrefabの読み込みに失敗しました。 Name = " + prefabName);

        CSVReader reader = new CSVReader();
        if (reader.Load(fileName))
        {
            for (int row = 0; row < reader.RowCount; ++row)
            {
                var obj = UnityEngine.Object.Instantiate(prefab, new Vector3(), Quaternion.identity) as GameObject;
                var enemy = obj.GetComponent<Enemy>();

                enemy.SpawnTime = reader.GetInt(row, 0);
                enemy.Pattern = reader.GetInt(row, 1);
                enemy.TypeId = (Enemy.Type)reader.GetInt(row, 2);
                enemy.X = reader.GetFloat(row, 3);
                enemy.Y = reader.GetFloat(row, 4);
                //enemy.Speed = reader.GetFloat(row, 5);
                float speed = reader.GetFloat(row, 5);
                enemy.ShotTime = reader.GetFloat(row, 6);
                enemy.ShotType = reader.GetInt(row, 7);
                enemy.BulletColor = reader.GetInt(row, 8);
                enemy.HP = reader.GetInt(row, 9);
                enemy.BulletType = reader.GetInt(row, 10);
                enemy.Wait = reader.GetFloat(row, 11);
                enemy.Point = reader.GetInt(row, 12);

                enemy.SetParameter(speed, new Vector2(enemy.X, enemy.Y));

                Enemy.target = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

                enemy.VanishCannotOverride();

                ReservedList.AddLast(enemy);
            }
        }
    }

    /// <summary>
    /// 敵機をアクティブにします。
    /// </summary>
    /// <param name="item"></param>
    public void AddActiveItem(Enemy item)
    {
        ReservedList.Remove(item);
        item.Revive();
        ActiveList.AddLast(item);
    }

    /// <summary>
    /// 敵機をアクティブリストから削除済みリストに移動させます。
    /// </summary>
    /// <param name="item"></param>
    public void RemoveActiveItem(Enemy item)
    {
        ActiveList.Remove(item);
        item.Vanish();
        RemovedList.AddLast(item);
    }

    /// <summary>
    /// 生存するインスタンスに対してラムダ式を実行する
    /// </summary>
    /// <param name="func"></param>
    public void ForEachExist(FuncT func)
    {
        foreach (var obj in ActiveList)
            func(obj);
    }

    /// <summary>
    /// 生存しているインスタンスをすべて破棄する
    /// </summary>
    public void Vanish()
    {
        ForEachExist(e => e.Vanish());
    }

}
