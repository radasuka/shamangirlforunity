﻿using UnityEngine;

public class BackScrolle : MonoBehaviour
{
    public float Speed = 1f;
    public GameObject[] Backgrounds = new GameObject[3];

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float amtToMove = Speed * Time.deltaTime;
        foreach (var obj in Backgrounds)
        {
            obj.transform.Translate(Vector3.back * amtToMove, Space.World);
            if (obj.transform.position.z <= -18f)
                obj.transform.position = new Vector3(transform.position.x, transform.position.y, 38.33f);
        }
    }
}
