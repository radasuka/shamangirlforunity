﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private static Score _instance = null;

    public Text ScoreText;
    public Text HighScoreText;
    private int score;
    private int highScore;

    /// <summary>
    /// 単一インスタンスを取得します
    /// </summary>
    public static Score Instance
    {
        get
        {
            // 初めて起動された場合
            if (_instance == null)
            {
                // インスタンスを生成
                _instance = FindObjectOfType(typeof(Score)) as Score;
                // インスタンスを生成することに失敗した場合
                if (_instance == null)
                {
                    Debug.LogError(typeof(Score) + "is nothing");
                }
            }
            return _instance;
        }
    }


    // Use this for initialization
    void Start()
    {
        score = 0;

        highScore = PlayerPrefs.GetInt("HighScoreKey", 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (highScore < score)
            highScore = score;

        ScoreText.text = score.ToString();
        HighScoreText.text = highScore.ToString();
    }

    /// <summary>
    /// スコアを加算
    /// </summary>
    /// <param name="value"></param>
    public void AddScore(int value)
    {
        score += value;
    }

    /// <summary>
    /// ハイスコアを保存
    /// </summary>
    public void Save()
    {
        PlayerPrefs.SetInt("HighScoreKey", highScore);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// ハイスコアをリセット
    /// </summary>
    public void Reset()
    {
        PlayerPrefs.DeleteAll();
    }
}
